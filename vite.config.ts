import preact from "@preact/preset-vite";
import {defineConfig} from "vite";
import checker from "vite-plugin-checker";

// https://vitejs.dev/config/
export default defineConfig({ // eslint-disable-line import/no-default-export
	plugins: [
		preact(),
		checker({
			typescript: true,
			eslint: {
				lintCommand: "eslint \"./**/*.{ts,tsx}\"",
			},
		}),
	],
	server: {
		strictPort: true,
		port: 5173,
		host: true,
		origin: "http://localhost:5173",
	},
	build: {
		manifest: true,
		outDir: "../../../media/mod_nfifilters",
		rollupOptions: {
			input: "./src/modules/mod_nfifilters/public/main.tsx",
		},
	},
	root: "./src/modules/mod_nfifilters/public",
});
