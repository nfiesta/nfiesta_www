function style(feature) {
    return {
        fillColor: getColor(feature.properties.lesnatost_bo),
        weight: 2,
        opacity: 1,
        color: '#252525',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#3fb1a9',
        dashArray: '',
        fillOpacity: 0.7
    });
//TODO Test browsers
//    if (!L.Browser.ie && !L.Browser.opera) {
//        layer.bringToFront();
//    }
    info.update(layer.feature.properties);
}

function resetHighlight(e) {
    nuts1Layer.resetStyle(e.target);
    info.update();
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

function onEachLayer(layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

function getColor(d) {
    return d > 41.0 ? '#005824' :
           d > 39.0 ? '#238b45' :
           d > 36.0 ? '#41ae76' :
           d > 33.0 ? '#66c2a4' :
           d > 31.0 ? '#99d8c9' :
                      '#ccece6';
}

