<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_nfidata
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

define( 'DS', DIRECTORY_SEPARATOR );

require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'configuration.php' );
require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'nfiajaxquery.php' );

/**
 * NfiData Model
 *
 * @since  0.0.1
 */
class NfiDataModelNfiData extends JModelItem
{
	private $dbconfig;
	/**
	 * @var JSON response
	 */
	protected $response;

	public function getJson()
	{
		$jinput = JFactory::getApplication()->input;

		$language = JFactory::getLanguage()->getTag();

		if (!isset($this->response))
		{
			//Databaze
			$config = new NFIConfig;
			$db = new NFIAjaxQuery;
			$db->connect($config->db_config);

			$dataset = $jinput->get('dataset', null, 'STRING');
			if ($dataset === 'basic-numerator') {
				$this->response = $this->getBasicNumeratorJson($db, $language, $jinput);
			} elseif ($dataset === 'unit-of-measurement') {
				$this->response = $this->getUnitOfMeasurementJson($db, $language, $jinput);
			} elseif($dataset === 'basic-denominator') {
				$this->response = $this->getBasicDenominatorJson($db, $language, $jinput);
			} elseif ($dataset === 'local-density-core-numerator') {
				$this->response = $this->getLocalDensityCoreNumeratorJson($db, $language, $jinput);
			} elseif ($dataset === 'local-density-core-denominator') {
				$this->response = $this->getLocalDensityCoreDenominatorJson($db, $language, $jinput);
			} elseif ($dataset === 'local-density-division-numerator') {
				$this->response = $this->getLocalDensityDivisionNumeratorJson($db, $language, $jinput);
			} elseif ($dataset === 'local-density-division-denominator') {
				$this->response = $this->getLocalDensityDivisionDenominatorJson($db, $language, $jinput);
			} elseif($dataset === 'area-domain-numerator') {
				$this->response = $this->getAreaDomainNumeratorJson($db, $language, $jinput);
			}  elseif($dataset === 'area-domain-denominator') {
				$this->response = $this->getAreaDomainDenominatorJson($db, $language, $jinput);
			} elseif($dataset === 'sub-population-numerator') {
				$this->response = $this->getSubPopulationNumeratorJson($db, $language, $jinput);
			} elseif($dataset === 'sub-population-denominator') {
				$this->response = $this->getSubPopulationDenominatorJson($db, $language, $jinput);
			} elseif($dataset === 'metadata') {
				$this->response = $this->getMetadata($db, $language, $jinput);
			} elseif($dataset === 'translations') {
				$this->response = $this->getTranslations($db, $language);
			}

			$db->close();
		}

		return $this->response;
	}

	private function getPreparedGraphJson($db,$jtag,$prepared_query, $attribute) {
		$estimateData = $db->getPreparedEstimateGraph ($jtag,$prepared_query, $attribute);

		$data = array_values ( pg_fetch_all ( $estimateData ) );

		$color = $db->getColorForAdomain ($attribute);

		$returnData = array(
			'color' => $color,
			'data' => $data
		);

		return json_encode($returnData);
	}

	private function getGraphJson($db,$jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type,$attribute) {
		$estimateData = $db->getEstimateGraph ($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type,$attribute);

		$data = array_values ( pg_fetch_all ( $estimateData ) );

		$color = $db->getColorForAdomain ($attribute);

		$returnData = array(
				'color' => $color,
				'data' => $data
		);

		return json_encode($returnData);
	}

	private function getMapJson($db,$jtag,$prepared_query) {
		$estimateData = $db->getPreparedEstimateMap ($jtag,$prepared_query);

		return json_encode($estimateData);
	}

	private function getBasicNumeratorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'topic' => $jinput->get('topic', null, 'int'),
			'period' => $jinput->get('period', null, 'int'),
			'geographicRegion' => $jinput->get('geographicRegion', null, 'int'),
			'group' => $jinput->get('group', null, 'string'),
			'indicator' => $jinput->get('indicator', null, 'bool'),
			'stateOrChange' => $jinput->get('stateOrChange', null, 'bool'),
			'unit' => $jinput->get('unit', null, 'bool'),
		];

		return json_encode($db->getBasicNumerator($language, $data));
	}

	private function getUnitOfMeasurementJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'topic' => 1,
			'period' => 1,
			'geographicRegion' => 1,
			'group' => $jinput->get('group', null, 'string'),
			'indicator' => true,
			'stateOrChange' => true,
			'unit' => true,
		];

		return json_encode($db->getBasicNumerator($language, $data));
	}

	private function getBasicDenominatorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
			'indicator' => $jinput->get('indicator', null, 'bool'),
		];

		return json_encode($db->getBasicDenominator($language, $data));
	}

	private function getLocalDensityCoreNumeratorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
		];

		return json_encode($db->getLocalDensityCoreNumerator($language, $data));
	}

	private function getLocalDensityCoreDenominatorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
		];

		return json_encode($db->getLocalDensityCoreDenominator($language, $data));
	}

	private function getLocalDensityDivisionNumeratorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
		];

		return json_encode($db->getLocalDensityDivisionNumerator($language, $data));
	}

	private function getLocalDensityDivisionDenominatorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
		];

		return json_encode($db->getLocalDensityDivisionDenominator($language, $data));
	}

	private function getAreaDomainNumeratorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
			'domain' => $jinput->get('domain', null, 'string'),
		];

		return json_encode($db->getAreaDomainNumerator($language, $data));
	}

	private function getAreaDomainDenominatorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
			'domain' => $jinput->get('domain', null, 'string'),
		];

		return json_encode($db->getAreaDomainDenominator($language, $data));
	}

	private function getSubPopulationNumeratorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
			'domain' => $jinput->get('domain', null, 'string'),
		];

		return json_encode($db->getSubPopulationNumerator($language, $data));
	}

	private function getSubPopulationDenominatorJson(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$data = [
			'group' => $jinput->get('group', null, 'string'),
			'domain' => $jinput->get('domain', null, 'string'),
		];

		return json_encode($db->getSubPopulationDenominator($language, $data));
	}

	private function getMetadata(NFIAjaxQuery $db, string $language, JInput $jinput): string {
		$group = $jinput->get('group', null, 'string');

		return json_encode($db->getUserMetadata($language, $group));
	}

	private function getTranslations(NFIAjaxQuery $db, string $language): string {
		return json_encode($db->getTranslations($language));
	}

}
