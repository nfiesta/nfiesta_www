<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_nfidata
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * JSON View
 *
 * @since  0.0.1
 */
class NfiDataViewNfiData extends JViewLegacy
{
	/**
	 * Display the NFI Data view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Get the document object.
		$document = JFactory::getDocument();
			
//		Set the MIME type for JSON output.
		$document->setMimeEncoding('application/json');
		
		$this->output = $this->get('Json');
 
		// Display the view
		parent::display($tpl);
	}
}