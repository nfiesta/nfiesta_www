<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_nfidata
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
/**
 * NFI Data Component Controller
 *
 * @since  0.0.1
 */
class NfiDataController extends JControllerLegacy
{
	public function execute($task)
	{
		try
		{
			$view = $this->getView( 'nfidata', 'json' );
			$view->setModel( $this->getModel( 'nfidata' ), true );
			$view->display();
		}
		catch(Exception $e)
		{
			echo new JResponseJson($e);
		}
	}
}
