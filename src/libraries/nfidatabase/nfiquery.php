<?php

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

require_once( dirname(__FILE__). DS .'basicquery.php');

// Getting language Tag from Joomla
$jlang = JFactory::getLanguage();
$jtag = $jlang->getTag();

/**
* NFI ajax queries
* @author Jan Bojko
*/
class NFIQuery extends basicquery {

	/**
	 * Get a description from the lookup table.
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getLookUp($tableName,$id) {

		$prepIterator = 1;

		if(!$id) {
			//TODO Error page
			header('Location:'.$this->baseurl);
		} else {
			$whereCondition = 'id = $'.$prepIterator;
		}

		$params = array($id);

		$sql = sprintf('SELECT label, description FROM %s WHERE %s', $tableName, $whereCondition);

		$result = $this->executePreparedQuery($sql,$params);

		if ($result) {
			$this->lastQueryResultResource = $result;
		}
		return $result;
	}

	/**
	 * Get attribute metadata with colors from prepared query id.
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getPreparedAttributeMetadata($pqAttributeMetadataFunction,$jtag,$prepared_query) {
	
		$params = array($jtag,$prepared_query);

		$sql = sprintf('SELECT * FROM %s($1,$2)', $pqAttributeMetadataFunction);

		$result = $this->executePreparedQuery($sql,$params);

		$resultArr = pg_fetch_all($result);

		return $resultArr;
	}

	/**
	 * Get attribute types in array form. Split PostgreSQL array in form of {1,2} into array of values 1 and 2 .
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getPreparedAttributeTypes($pqTable,$prepared_query) {

		$params = array($prepared_query);

		$sql = sprintf('SELECT attribute_type FROM %s WHERE id = $1', $pqTable);

		$result = $this->executePreparedQuery($sql,$params);

		$resultArr = pg_fetch_all($result);

		$explodedArr = explode(',',str_replace(array('{', '}'), '', $resultArr[0]['attribute_type']));

		return $explodedArr;
	}

	/**
	 * Get a row from an NFI estimate table with a specific column set for prepared where condition.
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getPreparedEstimate($pqFunction, $jtag, $prepared_query, $attributesCount, $graph, $cartodiagram, $export) {
	
		$params = array($jtag, $prepared_query, $graph, $cartodiagram, $export);

		if ($attributesCount == 1) {

            if($graph == 'true')
            {
                $sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(lower_limit::text, \'.\', \',\') as "3",replace(upper_limit::text, \'.\', \',\') as "4",replace(rerror::text, \'.\', \',\') as "5" FROM %s($1,$2,$3::boolean,$4::boolean,$5::boolean)', $pqFunction);            
            }
            else
            {
			    $sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(rerror::text, \'.\', \',\') as "3",replace(ssize::text, \'.\', \',\') as "4",replace(min_ssize::text, \'.\', \',\') as "5",CASE WHEN half_ci = \'NaN\' THEN \'NA\'::text ELSE replace(\'&plusmn;&nbsp;\' || half_ci::text, \'.\', \',\') END as "6" FROM %s($1,$2,$3::boolean,$4::boolean,$5::boolean)', $pqFunction);
            }

		} elseif ($attributesCount > 1) {

            if($graph == 'true')
            {
                //$sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(lower_limit::text, \'.\', \',\') as "3",replace(upper_limit::text, \'.\', \',\') as "4",replace(rerror::text, \'.\', \',\') as "5", regexp_replace(replace(attribute_label::text,\',\', \', \'), \'}|{|"\'::text, \'\'::text,\'g\'::text) as "6" FROM %s($1,$2,$3::boolean, $4::boolean)', $pqFunction);            
                $sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(lower_limit::text, \'.\', \',\') as "3",replace(upper_limit::text, \'.\', \',\') as "4",replace(rerror::text, \'.\', \',\') as "5", attribute_label_text as "6" FROM %s($1,$2,$3::boolean, $4::boolean,$5::boolean)', $pqFunction);
            }
            else
            {
			    //$sql = sprintf('SELECT gdomain_description as "0",regexp_replace(replace(attribute_label::text,\',\', \', \'), \'}|{|"\'::text, \'\'::text,\'g\'::text) as "1",replace(point_estimate::text, \'.\', \',\') as "2",replace(stderror::text, \'.\', \',\') as "3",replace(rerror::text, \'.\', \',\') as "4",replace(ssize::text, \'.\', \',\') as "5",replace(min_ssize::text, \'.\', \',\') as "6",CASE WHEN half_ci = \'NaN\' THEN \'NA\'::text ELSE replace(\'&plusmn;&nbsp;\' || half_ci::text, \'.\', \',\') END as "7" FROM %s($1,$2,$3::boolean,$4::boolean)', $pqFunction);
			    $sql = sprintf('SELECT gdomain_description as "0",attribute_label_text as "1",replace(point_estimate::text, \'.\', \',\') as "2",replace(stderror::text, \'.\', \',\') as "3",replace(rerror::text, \'.\', \',\') as "4",replace(ssize::text, \'.\', \',\') as "5",replace(min_ssize::text, \'.\', \',\') as "6",CASE WHEN half_ci = \'NaN\' THEN \'NA\'::text ELSE replace(\'&plusmn;&nbsp;\' || half_ci::text, \'.\', \',\') END as "7" FROM %s($1,$2,$3::boolean,$4::boolean,$5::boolean)', $pqFunction);
            }

		} else {
			die("Attributes are missing in the query '$sql': " . pg_last_error());
		}

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

	/**
	 * Get attribute metadata with colors from attribute_type.
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function  getUserAttributeMetadata($userAttributeMetadataFunction,$jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type) {

		$attribute_type_pgarray = $this->to_pg_array($attribute_type);

		$params = array($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type_pgarray);

		$sql = sprintf('SELECT * FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)', $userAttributeMetadataFunction);

		$result = $this->executePreparedQuery($sql,$params);

		$resultArr = pg_fetch_all($result);

		return $resultArr;
	}

	/**
	 * Get a row from an NFI estimate table with a specific column set.
	 */
	function getEstimate($userFunction,$attributesCount,$jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$graph,$export,$attribute_type) {

 	  /*
      if ($attribute_type == null)
    {
      die("die z nfiquery: attribute_type IS NULL");
    }
    else
    {
      //die("die z nfiquery: attribute_type: '$attribute_type' ");

 		     $attribute_type_pgarray = $this->to_pg_array($attribute_type);
         die("die z nfiquery: attribute_type: '$attribute_type_pgarray' ");
    }
    */


		$attribute_type_pgarray = $this->to_pg_array($attribute_type);

		$params = array($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$graph,$export,$attribute_type_pgarray);

		if ($attributesCount == 1) {

            if($graph == 'true')
            {
                $sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(lower_limit::text, \'.\', \',\') as "3",replace(upper_limit::text, \'.\', \',\') as "4",replace(rerror::text, \'.\', \',\') as "5" FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10::BOOLEAN,FALSE,$11::BOOLEAN,$12)',$userFunction);
            }
            else
            {
			    $sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(rerror::text, \'.\', \',\') as "3",replace(ssize::text, \'.\', \',\') as "4",replace(min_ssize::text, \'.\', \',\') as "5",CASE WHEN half_ci = \'NaN\' THEN \'NA\'::text ELSE replace(\'&plusmn;&nbsp;\' || half_ci::text, \'.\', \',\') END as "6" FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10::BOOLEAN,FALSE,$11::BOOLEAN,$12)', $userFunction);
            }

		} else if ($attributesCount > 1) {

            if($graph == 'true')
            {
                //$sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(lower_limit::text, \'.\', \',\') as "3",replace(upper_limit::text, \'.\', \',\') as "4",replace(rerror::text, \'.\', \',\') as "5",regexp_replace(replace(attribute_label::text,\',\', \', \'), \'}|{|"\'::text, \'\'::text,\'g\'::text) as "6" FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10::BOOLEAN,FALSE,$11)',$userFunction);
                $sql = sprintf('SELECT gdomain_description as "0", replace(point_estimate::text, \'.\', \',\') as "1",replace(stderror::text, \'.\', \',\') as "2",replace(lower_limit::text, \'.\', \',\') as "3",replace(upper_limit::text, \'.\', \',\') as "4",replace(rerror::text, \'.\', \',\') as "5",attribute_label_text as "6" FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10::BOOLEAN,FALSE,$11::BOOLEAN,$12)',$userFunction);
            }
            else
            {
                //$sql = sprintf('SELECT gdomain_description as "0",regexp_replace(replace(attribute_label::text,\',\', \', \'), \'}|{|"\'::text, \'\'::text,\'g\'::text) as "1",replace(point_estimate::text, \'.\', \',\') as "2",replace(stderror::text, \'.\', \',\') as "3",replace(rerror::text, \'.\', \',\') as "4",replace(ssize::text, \'.\', \',\') as "5",replace(min_ssize::text, \'.\', \',\') as "6",CASE WHEN half_ci = \'NaN\' THEN \'NA\'::text ELSE replace(\'&plusmn;&nbsp;\' || half_ci::text, \'.\', \',\') END as "7" FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10::BOOLEAN,FALSE,$11)', $userFunction);
                $sql = sprintf('SELECT gdomain_description as "0",attribute_label_text as "1",replace(point_estimate::text, \'.\', \',\') as "2",replace(stderror::text, \'.\', \',\') as "3",replace(rerror::text, \'.\', \',\') as "4",replace(ssize::text, \'.\', \',\') as "5",replace(min_ssize::text, \'.\', \',\') as "6",CASE WHEN half_ci = \'NaN\' THEN \'NA\'::text ELSE replace(\'&plusmn;&nbsp;\' || half_ci::text, \'.\', \',\') END as "7" FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10::BOOLEAN,FALSE,$11::BOOLEAN,$12)', $userFunction);
            }

		} else {
			die("Attributes are missing in the query '$sql': " . pg_last_error());
		}

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

	/**
	 * Get attribute metadata with colors from prepared query id.
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getMetadata($userMetadataFunction,$jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type) {

		$attribute_type_pgarray = $this->to_pg_array($attribute_type);

		$params = array($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type_pgarray);

		$sql = sprintf('SELECT * FROM %s($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)', $userMetadataFunction);

		$result = $this->executePreparedQuery($sql,$params);

		$resultArray = array();

		while ($row = pg_fetch_assoc($result)) {
			$resultArray[$row['data']] = array('label' => $row['label'], 'description' => $row['description']);
		}
	
		return $resultArray;
	}

	function getUserQuery(string $language, string $group) {
		$params = [$language, $group];

		$sql = sprintf('SELECT * FROM %s.fn_get_user_query($1,$2)', $this->dbconfig["data_scheme"]["name"]);

		return pg_fetch_all($this->executePreparedQuery($sql,$params));
	}

	function getUserMetadata(string $language, string $group) {
		$params = [$language, $group];

		$sql = sprintf('SELECT * FROM %s.fn_get_user_metadata($1,$2)', $this->dbconfig["data_scheme"]["name"]);

		$result = $this->executePreparedQuery($sql,$params);

		return json_decode(pg_fetch_row($result)[0]);
	}

	function getTranslations(string $language) {
		$params = [$language];

		$sql = sprintf('SELECT * FROM %s.fn_get_gui_headers($1)', $this->dbconfig["data_scheme"]["name"]);

		$result = $this->executePreparedQuery($sql,$params);

		return json_decode(pg_fetch_row($result)[0]);
	}

}
