<?php
class NFIConfig {

		public $file_config = array(
            "dir" => "./downloads",
            "types" => "pdf,jpeg"
        );

        public $db_config = array(
              "server" => "host.docker.internal",
              "port" => "5433",
              "database" => "postgres",
              "username" => "vagrant",
              "data_scheme" => array(
	              "name" => "nfiesta_results",
	              "pq_function" => "fn_get_prepared_query",
	              "pq_table" => "t_prepared_query",
	              "user_function" => "fn_get_user_query",
	              "pq_metadata_function" => "fn_get_prepared_metadata",
	              "user_metadata_function" => "fn_get_user_metadata",
	              "pq_attribute_metadata_function" => "fn_get_prepared_attribute_metadata",
	              "user_attribute_metadata_function" => "fn_get_user_attribute_metadata"
              ),
              "password" => "vagrant",
        );
}
?>
