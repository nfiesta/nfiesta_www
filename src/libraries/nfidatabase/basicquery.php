<?php

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

require_once( dirname(__FILE__). DS .'postgresql.php');
require_once( dirname(__FILE__). DS .'configuration.php');

/**
 * PHP Basic query class
 * NFI class with basic database queries
 * created by Jan Bojko
 */
class basicquery extends postgresql {

	private $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

	function __construct() {
		$config = new NFIConfig;
		$this->dbconfig = $config->db_config;
	}

	/**
	 * Return database color for adomain id, if not present return random
	 */
	function getColorForAdomain($attributes) {

		$colorsTable = $this->dbconfig["data_scheme"]["name"] . '.' . 'c_color AS col';
		$confColorsTable = $this->dbconfig["data_scheme"]["name"] . '.' . 't_attribute';

		$attribute = 1;
		
		if (is_array($attributes)) {
				foreach ($attributes as $att) {
					//if ($att != 1) {
						$attribute = $att;
					//}
				}
		} else {
      //die("attributes neni pole");
			$attribute = $attributes;
		}
    
    //die("die z basicquery: '$attribute' ");
		
		//$sql = sprintf('SELECT col.label FROM ' . $colorsTable . ', ' . $confColorsTable . ' AS att WHERE att.color = col.id AND att.id_attribute = %d', $attribute);
		$sql = sprintf('SELECT col.label FROM ' . $colorsTable . ', (SELECT 1 AS id_attribute, 600 AS color UNION ALL SELECT id_attribute, color FROM ' . $confColorsTable . ') AS att WHERE att.color = col.id AND att.id_attribute = %d', $attribute);

		$result = $this->executeQuery($sql);
		
		if (pg_numrows($result) > 0) {
			$dbColor = pg_fetch_result($result, 0, 0);
		} else {
			$dbColor = null;
		}
		
		//Get color from the database otherwise get random color
		if($dbColor) {
			$color = $dbColor;
		} else {
			$color = '#'.$this->rand[rand(0,15)].$this->rand[rand(0,15)].$this->rand[rand(0,15)].$this->rand[rand(0,15)].$this->rand[rand(0,15)].$this->rand[rand(0,15)];
		}

		return $color;
	}

	/**
	 * Get metadata from prepared query id.
	 * Create assoc array from the result.
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getPreparedMetadata($pqMetadataFunction,$jtag,$prepared_query) {

		$params = array($jtag,$prepared_query);

		$sql = sprintf('SELECT * FROM %s($1,$2)', $pqMetadataFunction);

		$result = $this->executePreparedQuery($sql,$params);

		$resultArray = array();

		while ($row = pg_fetch_assoc($result)) {
			$resultArray[$row['data']] = array('label' => $row['label'], 'description' => $row['description']);
		}

		return $resultArray;
	}

	/**
	 * Prepare attributes in the form of an array. Variables has to be ordered as follows nameN, where N is 1...X
	 * @param jinput table
	 * @param str where
	 */
	public function getArrayFromOrderedVariables($jinput, $name) {
		$num = 1;
		$returnArray = array();
		//0 value will be null
		while ($jinput->get($name.$num, null, 'INT')) {
			array_push($returnArray, $jinput->get($name.$num, null, 'INT'));
			$num = $num + 1;
		}

		if (empty($returnArray)) {
			$returnArray = null;
		}

		return $returnArray;
	}
}
?>