<?php

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

require_once( dirname(__FILE__). DS .'basicquery.php');

/**
* NFI ajax queries
* @author Jan Bojko
*/
class NFIAjaxQuery extends basicquery {

	/**
	 * Get a row from an NFI estimate table with a specific column set for prepared where condition.
	 * Map graph json structure
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getPreparedEstimateMap($jtag,$prepared_query) {

		$params = array($jtag,$prepared_query);
		//$sql = sprintf('SELECT gdomain, attribute, regexp_replace(attribute_label::text, \'}|{|"\'::text, \'\'::text,\'g\'::text) as attribute_label, point_estimate, lower_limit, upper_limit FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . $this->dbconfig["data_scheme"]["pq_function"] . '($1,$2::integer,FALSE,TRUE,NULL)');
        $sql = sprintf('SELECT gdomain, attribute, attribute_label_text as attribute_label, point_estimate, lower_limit, upper_limit FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . $this->dbconfig["data_scheme"]["pq_function"] . '($1,$2::integer,FALSE,TRUE,TRUE,NULL)');

		$result = $this->executePreparedQuery($sql,$params);

		$resultArray = array_values(pg_fetch_all($result));

		$categories = array();

		foreach ($resultArray as $value) {
			if (!isset( $categories[$value["gdomain"]] )) {
				$categories[$value["gdomain"]] = array();
			}

			$category = array();
			$category["point_estimate"] = $value["point_estimate"];
			$category["lower_limit"] = $value["lower_limit"];
			$category["upper_limit"] = $value["upper_limit"];
			$category["attributeid"] = $value["attribute"];


			array_push($categories[$value["gdomain"]],$category);
		}

		$sql = sprintf('SELECT gdomain, sum(point_estimate) FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . $this->dbconfig["data_scheme"]["pq_function"] . '($1,$2::integer,FALSE,TRUE,TRUE,NULL) WHERE gdomain != 100 GROUP BY gdomain');
		$result = $this->executePreparedQuery($sql,$params);

		$resultArray = array_values(pg_fetch_all($result));

		$sums = array();

		foreach ($resultArray as $value) {
			$sums[$value["gdomain"]] = $value["sum"];
		}

		$data = array();
		$cat = array();
		foreach ($resultArray as $key => $value) {
			$gdom = $resultArray[$key]['gdomain'];
			$cat['sumOfPointEstimate'] = $resultArray[$key]['sum'];
			$cat['gdomain'] = $gdom;
			$cat['categories'] = $categories[$gdom];

			array_push($data,$cat);
		}

		//Pocet trid 1 + 3,3*log(n)
		//Pro kraje 4 tridy
		$catNum = 4;
		$sql = sprintf('SELECT (max(stats.sum) - min(stats.sum)) / %d as coef , min(stats.sum) , max(stats.sum) FROM (%s) as stats', $catNum, $sql);

		$result = $this->executePreparedQuery($sql,$params);
		$resultArray = array_values(pg_fetch_all($result));

    // -----------------------------------------------------------------------------------
    // uprava rozsashu do legendy pro kolacovy graf - DEFAULTNI nastaveni pro % vystupy
    // proces ziskani unit of measure label
    // pokud jde o procento %, pak nize bude
    // pro $min nastavena defultni hodnota 0
    // pro $coef nastavena defaultni hodnota 25
    $pqMetadataFunction = $this->dbconfig["data_scheme"]["name"] .  '.' . $this->dbconfig["data_scheme"]["pq_metadata_function"];
		$metadatadefault = $this->getPreparedMetadata($pqMetadataFunction,$jtag,$prepared_query);
    $unitOfMeasuredefault = $metadatadefault['unit_of_measure']['label'];
    // ------------------------------------------------------------------------------------

		$legend = array();

    if ($unitOfMeasuredefault == '%') {
    $min = 0;
    $coef = 25;
    }
    else
    {
		$min = $resultArray[0]['min'];
		$coef = ceil($resultArray[0]['coef']);
    }

		$next = $min + $coef;

		for ($i = 0; $i < $catNum; $i++) {
			$size = 14 + ($i*4);
			$legend[$i]['size'] = intval($size);

		    $legend[$i]['lower'] = intval($min);
		    $legend[$i]['upper'] = intval($next-1);

		    if($i == $catNum-1) {
		    	$legend[$i]['upper'] += 1;
		    }

		    $min += $coef;
		    $next = $min + $coef;

		};

		//$sql = sprintf('SELECT attribute, regexp_replace(attribute_label::text, \'}|{|"\'::text, \'\'::text,\'g\'::text) as attribute_label FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . $this->dbconfig["data_scheme"]["pq_function"] . '($1,$2::integer,FALSE,TRUE,NULL) GROUP BY attribute, attribute_label');
		$sql = sprintf('SELECT attribute, attribute_label_text as attribute_label FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . $this->dbconfig["data_scheme"]["pq_function"] . '($1,$2::integer,FALSE,TRUE,TRUE,NULL) GROUP BY attribute, attribute_label_text');

		$result = $this->executePreparedQuery($sql,$params);
		$resultArray = array_values(pg_fetch_all($result));
		$attributes = array();

		foreach ($resultArray as $value) {
			$attribute = array();

      // puvodni spatne volani funkce pro ziskani barvy pro dany atribut
      // argument attribute zde vstupuje jako string obaleny {}, napr. {701}
			// $color = $this->getColorForAdomain($value['attribute']);
      // provedena uprava:
      $color_string = $value['attribute'];
      $replace = array("{", "}");
      $color4getColorForAdomain = str_replace($replace, "", $color_string);
      $color = $this->getColorForAdomain($color4getColorForAdomain);

      //die("die z nfiajaxquery: '$color' ");

			$attribute['label'] = $value['attribute_label'];
			$attribute['color'] = $color;
			$attribute['attributeid'] = $value['attribute'];
			array_push($attributes, $attribute);
		};

		$pqMetadataFunction = $this->dbconfig["data_scheme"]["name"] .  '.' . $this->dbconfig["data_scheme"]["pq_metadata_function"];
		$metadata = $this->getPreparedMetadata($pqMetadataFunction,$jtag,$prepared_query);

		$unitOfMeasure = $metadata['unit_of_measure']['label'];
		$description = $metadata['result']['label'];

		$mapOutputArray = array();
		//TODO not used yet
		$mapOutputArray['title'] = $description;
		//FIXME vhodnejsi vzit z databaze
		$mapOutputArray['legend']['title'] =  ucfirst($description);
		$mapOutputArray['legend']['units'] = $unitOfMeasure;
		$mapOutputArray['legend']['scales'] = $legend;
		$mapOutputArray['legend']['attribute'] = $attributes;
		$mapOutputArray['data']['categories'] = $data;

		return $mapOutputArray;
	}

	/**
	 * Get a row from an NFI estimate table with a specific column set for prepared where condition.
	 * Graph json structure
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getPreparedEstimateGraph($jtag,$prepared_query,$attribute) {

		$attribute_pgarray = $this->to_pg_array($attribute);

		$params = array($jtag,$prepared_query,$attribute_pgarray);
		$sql = sprintf('SELECT gdomain_description as "0", point_estimate::text as "1", stderror::text as "2", lower_limit::text as "3",upper_limit::text as "4",rerror::text as "5" FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . $this->dbconfig["data_scheme"]["pq_function"] .'($1,$2::integer,TRUE,FALSE,TRUE,$3)');

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

	/**
	 * Get a row from an NFI estimate table with a specific column set for prepared where condition.
	 * Graph json structure
	 * @param str table
	 * @param str where
	 * @return resource A resultset resource
	 */
	function getEstimateGraph($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type,$attribute) {

		//Pro graf se pouziva hodnota true
		$graph = 'true';

		$attribute_type_pgarray = $this->to_pg_array($attribute_type);

		$attribute_pgarray = $this->to_pg_array($attribute);

		$params = array($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$graph,$attribute_type_pgarray,$attribute_pgarray);

		$sql = sprintf('SELECT gdomain_description as "0", point_estimate::text as "1", stderror::text as "2", lower_limit::text as "3",upper_limit::text as "4",rerror::text as "5" FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . 'fn_get_user_query($1,$2::integer,$3::integer,$4::integer,$5::integer,$6::integer,$7::integer,$8::integer,$9::integer,$10,FALSE,TRUE,$11,$12)');

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

	function getBasicNumerator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['topic'],
			$filter['period'],
			$filter['geographicRegion'],
			$filter['group'],
			$filter['indicator'],
			$filter['stateOrChange'],
			$filter['unit'],
		];

		return $this->getData('fn_get_user_options_numerator', $params);
	}

	public function getBasicDenominator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
			$filter['indicator'],
		];

		return $this->getData('fn_get_user_options_denominator', $params);
	}

	function getLocalDensityCoreNumerator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
		];

		return $this->getData('fn_get_user_options_numerator_ldsity_core', $params);
	}

	function getLocalDensityCoreDenominator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
		];

		return $this->getData('fn_get_user_options_denominator_ldsity_core', $params);
	}

	function getLocalDensityDivisionNumerator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
		];

		return $this->getData('fn_get_user_options_numerator_ldsity_division', $params);
	}

	function getLocalDensityDivisionDenominator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
		];

		return $this->getData('fn_get_user_options_denominator_ldsity_division', $params);
	}

	function getAreaDomainNumerator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
			$filter['domain'],
		];

		return $this->getData('fn_get_user_options_numerator_area_domain', $params);
	}

	public function getAreaDomainDenominator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
			$filter['domain'],
		];

		return $this->getData('fn_get_user_options_denominator_area_domain', $params);
	}

	function getSubPopulationNumerator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
			$filter['domain'],
		];

		return $this->getData('fn_get_user_options_numerator_sub_population', $params);
	}

	public function getSubPopulationDenominator(string $language, array $filter): array {
		$params = [
			$language,
			$filter['group'],
			$filter['domain'],
		];

		return $this->getData('fn_get_user_options_denominator_sub_population', $params);
	}

	private function getData(string $function, array $params): array {
		for ($i = count($params) - 1; $i > 0; $i--) {
			if ($params[$i] !== null) {
				break;
			}

			unset($params[$i]);
		}

		$paramsSubstitution = join(', ', array_map(function ($idx) { return '$' . ($idx + 1);}, array_keys($params)));
		$sql = sprintf("
			SELECT * 
			FROM {$this->dbconfig["data_scheme"]["name"]}.$function($paramsSubstitution)
		");

		$result = [];
		$rows = pg_fetch_all($this->executePreparedQuery($sql, $params));
		foreach ($rows as $row) {
			$data = [
				'label' => $row['res_label'],
				'id' => $row['res_id'],
				'group' => $row['res_id_group'],
				'detailed' => (
					$function === 'fn_get_user_options_numerator' && $row['res_id'] === '1'
					|| isset($row['res_division']) && $row['res_division'] === 't'
				),
			];

			if (array_key_exists('res_area_domain', $row)) {
				$data['domain'] = $row['res_area_domain'];
			}

			if (array_key_exists('res_sub_population', $row)) {
				$data['domain'] = $row['res_sub_population'];
			}

			$result[] = $data;
		}

		return $result;
	}

	function getUserMetadata(string $language, string $group) {
		$params = [$language, $group];

		$sql = sprintf('SELECT * FROM %s.fn_get_user_metadata($1,$2)', $this->dbconfig["data_scheme"]["name"]);

		$result = $this->executePreparedQuery($sql,$params);

		return json_decode(pg_fetch_row($result)[0]);
	}

	function getTranslations(string $language) {
		$params = [$language];

		$sql = sprintf('SELECT * FROM %s.fn_get_gui_headers($1)', $this->dbconfig["data_scheme"]["name"]);

		$result = $this->executePreparedQuery($sql,$params);

		return json_decode(pg_fetch_row($result)[0]);
	}

	/**
	 * Get id and label of next user category list of values.
	 * Filter json structure
	 * @return resource A resultset resource
	 */
	function getUserCategory($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type,$attribute) {

		$attribute_type_pgarray = $this->to_pg_array($attribute_type);

		$attribute_pgarray = $this->to_pg_array($attribute);

		$params = array($jtag,$result_type,$result,$period,$unit_of_measure,$population,$definition_variant,$area_domain,$gdomain_level,$attribute_type_pgarray,$attribute_pgarray);

		$sql = sprintf('SELECT res_id, res_label FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . 'fn_get_user_category($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)');

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

	/**
	 * Get id and label of next prepared category list of values.
	 * Filter json structure
	 * @return resource A resultset resource
	 */
	function getPreparedCategory($jtag, $pq, $attribute) {

		$attribute_pgarray = $this->to_pg_array($attribute);

		$params = array($jtag,$pq,$attribute_pgarray);

		$sql = sprintf('SELECT res_id, res_label FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . 'fn_get_prepared_category($1,$2,$3)');

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

	/**
	 * Get id and label of next user specified list of values.
	 * Filter json structure
	 * @return resource A resultset resource
	 */
	function getPreparedOptions($jtag,$period) {

		$params = array($jtag,$period);

		$sql = sprintf('SELECT res_id, res_label FROM ' . $this->dbconfig["data_scheme"]["name"] . '.' . 'fn_get_prepared_options($1,$2)');

		$result = $this->executePreparedQuery($sql,$params);

		return $result;
	}

}

?>
