function getNext(id) {
	switch (id) { 
		case 'period': 
			var next = "pc";
			var params = '"period"' + ':' + $( "#period option:selected" ).val();
			checkActive("pc");
			break;
	}
	return {
		next: next,
		params: params
	};
}

function checkActive(id) {
	switch (id) { 
		case 'period':
			$('#period').empty();
			$('#period').attr('disabled',true);
		case 'pc':
			$('#pc').empty();
			$('#pc').attr('disabled',true);
			$('#submit').attr('disabled',true);
	}
}

createFirstOption = function(){
	
	var mydata = $.parseJSON('{ "bf": 1 }');
	
	$.ajax({
		url: "index.php?option=com_nfidata&format=json",
		context: document.body, 
		data: mydata,
		dataType: 'json'
		}).done(function(data) {
			$('#period').append('<option id="notchosen">' + '...' + '</option>')
			$.each(data, function(idx, obj) {
				$('#period').append('<option value="' + obj.res_id + '">' + obj.res_label + '</option>')
			});
		});
};

changePreparedOption = function(id){
	
					var nextOptions = getNext(id);
					
					if($('#' + id + ' option:selected').attr('id')) {
						$('#' + nextOptions.next).attr('disabled', 'disabled');
						$('#' + nextOptions.next).empty();
						$('#submit').attr('disabled',true);
						return;
					}
					
					$('#' + nextOptions.next).empty();
					$('#' + nextOptions.next).removeAttr("disabled");
					
					var mydata = $.parseJSON('{ "bf": 1, ' + nextOptions.params + '}');
					
					$.ajax({
						url: "index.php?option=com_nfidata&format=json",
						context: document.body, 
        				data: mydata,
        				dataType: 'json'
						}).done(function(data) {
							$('#' + nextOptions.next).append('<option id="notchosen" disabled="disabled">' + '...' + '</option>')
							$.each(data, function(idx, obj) {
								$('#' + nextOptions.next).append('<option value="' + obj.res_id + '">' + obj.res_label + '</option>')
							});
							
							$('#' + nextOptions.next).find('option:first').attr("selected","selected");
						})
				};

commitChange = function(id){
					if($('#' + id + ' option:selected').attr('id')) {
						$('#submit').attr('disabled',true);
						return;
					}
					$('#submit').attr('disabled',false);
				};
