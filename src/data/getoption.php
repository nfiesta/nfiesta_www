<?php
if( $_REQUEST["pc"])
{
	if (is_numeric($_REQUEST['pc'])) {
		$prepared_query = $_REQUEST['pc'];
	} else {
		die();
	}
}

if( isset($_REQUEST["adomain1"]))
{
	if (is_numeric($_REQUEST['adomain1'])) {
		$adomain1 = $_REQUEST['adomain1'];
	}
}

$abs = null;

if(isset($_REQUEST['abs']))
{
	if ($_REQUEST['abs'] === "true") {
		$abs = $_REQUEST['abs'];
	}
}

require_once( dirname(__FILE__).'/configuration.php' );
require_once( dirname(__FILE__).'/database.php' );

$config = new NFIConfig;
$db = new postgresql;
$db->connect($config->db_config);

$tableName = 'results.view_t_nfi_estimate';
$pqTableName = 'results.w_prepared_query';
/*
 * Hodnoty pro graf, bude ziskana, pokud je adomain vyplneno
 */
if (isset($adomain1)) {
	$estimateData = $db->getPreparedEstimateGraph ( $tableName, $pqTableName, $prepared_query, $adomain1 ,$abs);
	
	$data = array_values ( pg_fetch_all ( $estimateData ) );
	
	echo json_encode($data);
} else {
	$estimateData = $db->getPreparedEstimateMap ( $tableName, $pqTableName, $prepared_query ,$abs);

	echo json_encode($estimateData);
}

$db->close();
