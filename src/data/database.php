<?php
/*
PHP REST SQL: A HTTP REST interface to relational databases
written in PHP
postgresql.php :: PostgreSQL database adapter
Copyright (C) 2008 Guido De Rosa <guidoderosa@gmail.com>
based on MySQL driver mysql.php by Paul James
Copyright (C) 2004 Paul James <paul@peej.co.uk>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
/* $id$ */
/**
* PHP REST PostgreSQL class
* PostgreSQL connection class.
*/
class postgresql {
/**
* @var int
*/
var $lastInsertPKeys;
/**
* @var resource
*/
var $lastQueryResultResource;
/**
* @var resource Database resource
*/
var $db;
/**
 * Connect to the database.
 * @param str[] config
 */
function connect($config) {
	$connString = sprintf(
			'host=%s dbname=%s user=%s password=%s',
			$config['server'],
			$config['database'],
			$config['username'],
			$config['password']
	);
	if ($this->db = pg_pconnect($connString)) {
		return TRUE;
	}
	return FALSE;
}
/**
 * Close the database connection.
 */
function close() {
	pg_close($this->db);
}
/**
 * Get the columns in a table.
 * @param str table
 * @return resource A resultset resource
 */
function getColumns($table) {
	$qs = sprintf('SELECT * FROM information_schema.columns WHERE table_name =\'%s\'', $table);
	return pg_query($qs, $this->db);
}
/**
 * Get a row from a table.
 * @param str table
 * @param str where
 * @return resource A resultset resource
 */
function getRow($table, $where) {
	$result = pg_query(sprintf('SELECT * FROM %s WHERE %s', $table, $where));
	if ($result) {
		$this->lastQueryResultResource = $result;
	}
	return $result;
}

function executePreparedQuery($sql, $params) {
	$sqlName = substr(uniqid('', true), -5);
	if (!pg_prepare ($sqlName, $sql)) {
		die("Can't prepare '$sql': " . pg_last_error());
	}
	$result = pg_execute($sqlName, $params);

	$sql = sprintf('DEALLOCATE "%s"', pg_escape_string($sqlName));
	if(!pg_query($sql)) {
		die("Can't query '$sql': " . pg_last_error());
	}

	if (pg_num_rows($result) == 0) {
		//TODO Error page
		die("No data query.");
	}

	return $result;
}

function executeQuery($sql) {
	$result = pg_query($sql);

	if(!$result) {
		die("Can't query '$sql': " . pg_last_error());
	}

	if ($result) {
		$this->lastQueryResultResource = $result;
	}

	return $result;
}

/**
 * Get a row from an NFI estimate table with a specific column set for prepared where condition.
 * Map graph json structure
 * @param str table
 * @param str where
 * @return resource A resultset resource
 */
function getPreparedEstimateMap($tableName,$pqTableName,$prepared_query,$abs) {

	//FIXME nemelo by byt natvrdo v kodu!!
	if ($abs === "true") {
		$gdomain = "AND cgdom.gdomain_type != 100 AND cgdom.gdomain_type != 200";
	}

	$params = array($prepared_query);

	$sql = sprintf('SELECT nfi_cycle,estimate,estimate_type,estimate_definition,gdomain,gdomain_type,adomain1,adomain2,adomain3,unit_of_measure,confidence_level,condition FROM %s WHERE id = $1', $pqTableName);

	$result = $this->executePreparedQuery($sql, $params);

	$conditionsArr = pg_fetch_all($result);

	$array = $conditionsArr[0];
	$whereCondition = "";
	$sqlUnitOfMeasure;
	foreach($array as $key => $value) {
		if ($value and $key == "unit_of_measure") {
			$whereCondition .= "est." . $key . " = " . $value . " AND ";
			$sqlUnitOfMeasure = sprintf('SELECT label FROM results.c_unit_of_measure WHERE id = %d', $value);
		}
		else if ($value and $key != "condition") {
			$whereCondition .= "est." . $key . " = " . $value . " AND ";
		} else if ($key == "condition" and $value != "") {
			$whereCondition .= "est." . $value . " AND ";
		}
	}

	$whereCondition = rtrim($whereCondition, " AND ");

	if($whereCondition == "") {
		die("Can't find prepared estimate.");
	}

	$sql = sprintf('SELECT est.gdomain, cadom.id as adomid, cadom.label as adom, est.point_estimate, est.lower_limit, est.upper_limit
  FROM results.t_nfi_estimate est, results.c_gdomain cgdom, results.c_adomain cadom 
  WHERE cadom.id = est.adomain1 AND cgdom.id = est.gdomain AND %s %s', $whereCondition, $gdomain);

	$result = $this->executeQuery($sql);
	$resultArray = array_values(pg_fetch_all($result));


	$categories = array();

	foreach ($resultArray as $value) {
		if (!isset( $categories[$value["gdomain"]] )) {
			$categories[$value["gdomain"]] = array();
		}

		$category = array();
		$category["point_estimate"] = $value["point_estimate"];
		$category["lower_limit"] = $value["lower_limit"];
		$category["upper_limit"] = $value["upper_limit"];
		$category["adomid"] = $value["adomid"];


		array_push($categories[$value["gdomain"]],$category);
	}

	$sql = sprintf('SELECT est.gdomain, sum(est.point_estimate) 
				FROM results.t_nfi_estimate est, results.c_gdomain cgdom 
				WHERE cgdom.id = est.gdomain AND %s %s GROUP BY est.gdomain', $whereCondition, $gdomain);

	$result = $this->executeQuery($sql);
	$resultArray = array_values(pg_fetch_all($result));

	$sums = array();

	foreach ($resultArray as $value) {
		$sums[$value["gdomain"]] = $value["sum"];
	}

	$data = array();
	$cat = array();
	foreach ($resultArray as $key => $value) {
		$gdom = $resultArray[$key]['gdomain'];
		$cat['sumOfPointEstimate'] = $resultArray[$key]['sum'];
		$cat['gdomain'] = $gdom;
		$cat['categories'] = $categories[$gdom];

		array_push($data,$cat);
	}

	//Pocet trid 1 + 3,3*log(n)
	//Pro kraje 4 tridy
	$catNum = 4;
	$sql = sprintf('SELECT (max(stats.sum) - min(stats.sum)) / %d as coef , min(stats.sum) , max(stats.sum) FROM (%s) as stats', $catNum, $sql);

	$result = $this->executeQuery($sql);
	$resultArray = array_values(pg_fetch_all($result));

	$legend = array();
	$min = $resultArray[0]['min'];
	$coef = ceil($resultArray[0]['coef']);
	$next = $min + $coef;

	for ($i = 0; $i < $catNum; $i++) {
		$size = 14 + ($i*4);
		$legend[$i]['size'] = $size;

	    $legend[$i]['lower'] = $min;
	    $legend[$i]['upper'] = $next-1;

	    if($i == $catNum-1) {
	    	$legend[$i]['upper'] += 1;
	    }

	    $min += $coef;
	    $next = $min + $coef;

	};

	$sql = sprintf('SELECT cadom.id as adomid, cadom.label as adom
		FROM results.t_nfi_estimate est, results.c_gdomain cgdom, results.c_adomain cadom
		WHERE cadom.id = est.adomain1 AND cgdom.id = est.gdomain AND %s %s GROUP BY cadom.id, adom', $whereCondition, $gdomain);

	$result = $this->executeQuery($sql);
	$resultArray = array_values(pg_fetch_all($result));

	$attributes = array();
	$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

	foreach ($resultArray as $value) {
		$attribute = array();
		$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
		$attribute['label'] = $value['adom'];
		$attribute['color'] = $color;
		$attribute['adomid'] = $value['adomid'];
		array_push($attributes, $attribute);
	};


	$sqlUnitOfMeasure;
	$result = $this->executeQuery($sqlUnitOfMeasure);
	$resultArray = array_values(pg_fetch_row($result));
	$unitOfMeasure = $resultArray[0];

	$sql = sprintf('SELECT description FROM %s WHERE id = $1', $pqTableName);
	$result = $this->executePreparedQuery($sql, $params);
	$resultArray = array_values(pg_fetch_row($result));
	$description = $resultArray[0];

	//TODO done
	$mapOutputArray = array();
	$mapOutputArray['title'] = $description;
	//FIXME vhodnejsi vzit z databaze
	$mapOutputArray['legend']['title'] = 'Celkem';
	$mapOutputArray['legend']['units'] = $unitOfMeasure;
	$mapOutputArray['legend']['scales'] = $legend;
	$mapOutputArray['legend']['attribute'] = $attributes;
	$mapOutputArray['data']['categories'] = $data;

	return $mapOutputArray;
}

/**
 * Get a row from an NFI estimate table with a specific column set for prepared where condition.
 * Graph json structure
 * @param str table
 * @param str where
 * @return resource A resultset resource
 */
function getPreparedEstimateGraph($tableName,$pqTableName,$prepared_query,$adomain1,$abs) {

	$params = array($prepared_query);

	//FIXME nemelo by byt natvrdo v kodu!!
	$gdomain = "AND gdomain_type != 200";

	//FIXME nemelo by byt natvrdo v kodu!!
	if ($abs === "true") {
		$gdomain = "AND gdomain_type != 100 AND gdomain_type != 200";
	}

	$sql = sprintf('SELECT nfi_cycle,estimate,estimate_type,estimate_definition,gdomain,gdomain_type,adomain1,adomain2,adomain3,unit_of_measure,confidence_level,condition FROM %s WHERE id = $1', $pqTableName, $adomain1);

	$result = $this->executePreparedQuery($sql, $params);

	$conditionsArr = pg_fetch_all($result);
	$array = $conditionsArr[0];
	$whereCondition = "";
	foreach($array as $key => $value) {
		if ($value and $key != "condition") {
			$whereCondition .= $key . " = " . $value . " AND ";
		}
	}

	$whereCondition = rtrim($whereCondition, " AND ");


	if($whereCondition == "") {
		die("Can't find prepared estimate.");
	}

	$sql = sprintf('SELECT cgdom_desc as "0", point_estimate::text as "1", lower_limit::text as "2",upper_limit::text as "3" FROM %s WHERE %s AND adomain1 = %d %s ORDER BY adomain1, gdomain', $tableName, $whereCondition, $adomain1, $gdomain);

	$result = $this->executeQuery($sql);

	return $result;
}

/**
 * Get the rows in a table.
 * @param str primary The names of the primary columns to return
 * @param str table
 * @return resource A resultset resource
 */
function getTable($primary, $table) {
	$result = pg_query(sprintf('SELECT %s FROM %s', $primary, $table));
	if ($result) {
		$this->lastQueryResultResource = $result;
	}
	return $result;
}
/**
 * Get the tables in a database.
 * @return resource A resultset resource
 */
function getDatabase() {
	return pg_query('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\'');
}
/**
 * Get the primary keys for the request table.
 * @return str[] The primary key field names
 */
function getPrimaryKeys($table) {
	$i = 0;
	$primary = NULL;
	do {
		$query = sprintf('SELECT pg_attribute.attname
				FROM pg_class, pg_attribute, pg_index
				WHERE pg_class.oid = pg_attribute.attrelid AND
				pg_class.oid = pg_index.indrelid AND
				pg_index.indkey[%d] = pg_attribute.attnum AND
				pg_index.indisprimary = \'t\'
and relname=\'%s\'',
		$i,
		$table
		);
		$result = pg_query($query);
		$row = pg_fetch_assoc($result);
		if ($row) {
			$primary[] = $row['attname'];
		}
		$i++;
	} while ($row);
	return $primary;
}
/**
 * Update a row.
 * @param str table
 * @param str values
 * @param str where
 * @return bool
 */
function updateRow($table, $values, $where) {
	# translate from MySQL syntax :)
	$values = preg_replace('/"/','\'',$values);
	$values = preg_replace('/`/','"',$values);
	$qs = sprintf('UPDATE %s SET %s WHERE %s', $table, $values, $where);
	$result = pg_query($qs);
	if ($result) {
		$this->lastQueryResultResource = $result;
	}
	return $result;
}
/**
 * Insert a new row.
 * @param str table
 * @param str names
 * @param str values
 * @return bool
 */
function insertRow($table, $names, $values) {
	# translate from MySQL syntax
	$names = preg_replace('/`/', '"', $names); #backticks r so MySQL-ish! ;)
	$values = preg_replace('/"/', '\'', $values);
	$pkeys = join(', ', $this->getPrimaryKeys($table));
	$qs = sprintf(
			'INSERT INTO $table ("%s") VALUES ("%s") RETURNING (%s)',
			$names,
			$values,
			$pkeys
	);
	$result = pg_query($qs); #or die(pg_last_error());
	$lastInsertPKeys = pg_fetch_row($result);
	$this->lastInsertPKeys = $lastInsertPKeys;
	if ($result) {
		$this->lastQueryResultResource = $result;
	}
	return $result;
	}
	/**
	 * Get the columns in a table.
	 * @param str table
	 * @return resource A resultset resource
	 */
	function deleteRow($table, $where) {
		$result = pg_query(sprintf('DELETE FROM %s WHERE %s', $table, $where));
		if ($result) {
			$this->lastQueryResultResource = $result;
		}
		return $result;
	}
	/**
	 * Escape a string to be part of the database query.
	 * @param str string The string to escape
	 * @return str The escaped string
	 */
	function escape($string) {
		return pg_escape_string($string);
	}
	/**
	 * Fetch a row from a query resultset.
* @param resource resource A resultset resource
* @return str[] An array of the fields and values from the next row in the resultset
*/
function row($resource) {
	return pg_fetch_assoc($resource);
}
/**
* The number of rows in a resultset.
* @param resource resource A resultset resource
* @return int The number of rows
*/
function numRows($resource) {
	return pg_num_rows($resource);
}
/**
* The number of rows affected by a query.
* @return int The number of rows
*/
function numAffected() {
	return pg_affected_rows($this->lastQueryResultResource);
}
/**
* Get the ID of the last inserted record.
* @return int The last insert ID ('a/b' in case of multi-field primary key)
*/
function lastInsertId() {
	return join('/', $this->lastInsertPKeys);
}
}
?>

