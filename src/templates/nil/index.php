<?php
defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

  $menu = $app->getMenu()->getActive();
  $pageclass = '';

  if (is_object($menu))
{
    $pageclass = $menu->params->get('pageclass_sfx');
}

$menuname = $app->getMenu()->getActive()->title;
$menudeff = array("About us","NFI results");
if (in_array($menuname,$menudeff)) { $pageclass = 'hp'; }

$jlang = JFactory::getLanguage();
$lang = $jlang->getTag();
include_once 'templates/nil/language/index_'.$lang.'.php';



  if (!defined('DS')) {
  	define( 'DS', DIRECTORY_SEPARATOR );
  }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=11" >
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" type="text/css" href="templates/nil/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="templates/nil/css/font.css" />
        <link rel="stylesheet" type="text/css" href="templates/nil/css/global.css" />
        <link rel="stylesheet" type="text/css" href="templates/nil/css/bxslider.css" />
        <link rel="stylesheet" type="text/css" href="templates/nil/css/table.css" />
        <link rel="stylesheet" type="text/css" href="templates/nil/template.css" />
        <link rel="stylesheet" type="text/css" href="templates/nil/css/back-top.css" />
        <script type="text/javascript" src="templates/nil/script/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="templates/nil/script/standard.js"></script>
        <script type="text/javascript" src="templates/nil/plugins/fancybox.js"></script>
        <script type="text/javascript" src="templates/nil/plugins/jquery.fitvids.js"></script>
        <script type="text/javascript" src="templates/nil/plugins/jquery.bxslider.min.js"></script>
   		<!--Table active part-->
        <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <!--Map active part-->
        <script src="./mapa/leaflet/nuts3.geojson"></script>
        <script src="./mapa/leaflet/nuts1.geojson"></script>
        <link rel="stylesheet" href="./mapa/leaflet/leaflet.css" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="./mapa/leaflet/map.css" type="text/css">
        <link rel="stylesheet" href="./mapa/leaflet/easy-button.css" type="text/css">
        <script src="./mapa/leaflet/leaflet-src.js"></script>
        <script src="./mapa/leaflet/mapFunction.js"></script>
        <script src="./mapa/leaflet/easy-button.js"></script>
        <script type="text/javascript" src="templates/nil/script/bubble.js"></script>
        <jdoc:include type="head" />
    </head>
    <body class="<?php echo $pageclass ? htmlspecialchars($pageclass) : 'sp'; ?>">
      <?php require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'analyticstracking.php' ); ?>



       <div id="top-header">
        <div class="container">
<!--
          <a href="http://www.uhul.cz" target="_blank"><img src="templates/nil/img/logo_top_header.png">Ústav pro hospodářskou úpravu lesůandys nad Labem</a>
-->
	  <a href="http://<?php echo $lang['TOP_HEADER_COMPANY_LINK']; ?>" target="_blank"><img src="templates/nil/img/logo_top_header.png"><?php echo $lang['TOP_HEADER_COMPANY']; ?></a>
        </div>
      </div>
      <div id="header">
        <div class="container">
          <img id="logo" src="templates/nil/img/logo.png" alt="<?php echo $lang['HEADER_COMPANY_SHORT']; ?> | Národní inventarizace lesů" />
          <a id="logo-link" href="/" title="<?php echo $lang['HEADER_COMPANY_SHORT']; ?> | <?php echo $lang['HEADER_COMPANY_LONG']; ?>">&nbsp;</a>
          <div id="lang">
              <!--<a href="#" title="English">English</a>
              <span>/</span>
              <a href="#" title="Deutsch">Deutsch</a>
	      <span>/</span>-->
	      <jdoc:include type="modules" name="lang" />
	      <?php echo "" ?>
          </div>
          <div id="login"></div>
          <div id="search">
		<jdoc:include type="modules" name="search" />
          </div>
          <div id="menu">
          	<jdoc:include type="modules" name="menu" />
          </div>
        </div>
      </div>
      <div id="content" class="overflow-hidden">   
        <div id="progress-wrap"><div id="progress-bar"><span style="width: 100%;"></span></div></div>
        <div class="container opacity-0">
          	<jdoc:include type="message" />
            <jdoc:include type="component" />       
        </div>
      </div> 
      <div id="footer">
        <div class="dotted-line"></div>
        <div class="container">
            <div id="footer-left">
              	<a href="http://<?php echo $lang['TOP_HEADER_COMPANY_LINK']; ?>" title="<?php echo $lang['TOP_HEADER_COMPANY_LINK']; ?>"><img id="footer-uhul" src="templates/nil/img/footer_logo_uhul.png" alt="UHUL"/></a>
            </div>
            <div id="footer-right">
                <p>
                    <span>E-mail: <a href="mailto:nil@uhul.cz" title="nil@uhul.cz">nil@uhul.cz</a></span>
                </p>
            </div>
        </div>
      </div>
      <div id="copyright">
        <div class="container">
          <p id="copyright-description">© 2019 nil.uhul.cz, <?php echo $lang['COPYRIGHT_DESCRIPTION']; ?>.</p>
          <p>
<!--
            <a href="mapa-stranek" title="Mapa stránek">Mapa stránek</a>
            <a href="odkazy" title="Odkazy">Odkazy</a>
-->
	    <a href="<?php echo $lang['MAP_PAGE_HREF']; ?>" title="<?php echo $lang['MAP_PAGE_TITLE']; ?>"><?php echo $lang['MAP_PAGE_TITLE']; ?></a>
	    <a href="<?php echo $lang['LINKS_HREF']; ?>" title="<?php echo $lang['LINKS_TITLE']; ?>"><?php echo $lang['LINKS_TITLE']; ?></a>
          </p>
        </div>
      </div>
      <p id="back-top">
        <a href="" title="<?php echo $lang['BACK_TOP']; ?>"><span></span></a>
      </p>
<!-- Archiv aktualit bez tooltipu -->
		<script type="text/javascript">
			$(function () {
				$('.pagination a').removeClass('hasTooltip');
			});
		</script>
    </body>
</html>

