if (!app)
   var app = {};

app.bubbleHelp = {
   elem: null,
   hi: function(event) {
      var hoverElem = $(event.target).closest('.bubble-help-hover');
      app.bubbleHelp.elem = hoverElem.find('.bubble-wrap');
      if (app.bubbleHelp.elem.length == 0)
         app.bubbleHelp.elem = hoverElem.find('.bubble-help')
            .wrap('<div class=bubble-wrap></div>')
            .parent().append('<div>&#9660;</div>');
      app.bubbleHelp.elem.find('.bubble-help').show();
      app.bubbleHelp.elem.css('top', -app.bubbleHelp.elem.height())
         .hide().fadeIn();
      },
   bye: function() {
      app.bubbleHelp.elem.fadeOut('slow');
      },
   setup: function() {
      $('.bubble-help').parent().addClass('bubble-help-hover')
         .hover(this.hi, this.bye);
      }
   };

$(function() {
   app.bubbleHelp.setup();
   });