$(document).ready(function(){
      $('img').each(function () {
        $(this).load(function () {
          $(this).show();
        });
      });	  
  
  	  var windowHeight = $('#header').outerHeight() + $('#content').outerHeight() +
          $('#footer').outerHeight() + $('#copyright').outerHeight();
  	  if ($(window).outerHeight() > windowHeight) {
        	$('#content .container').css('min-height', $('#content').outerHeight() + $(window).outerHeight() - windowHeight - 60);
      }      
		if($( ".fancybox" ).length > 0 ){
			  $(".fancybox").fancybox({
				'transitionIn': 'elastic',
				'transitionOut': 'elastic',
				'speedIn': 600,
				'speedOut': 200,
				'overlayShow': true,
				'showCloseButton': true,
				'centerOnScroll': true
			  });
		}
	  if($( ".bxslider_hp" ).length > 0 ){
	      $('.bxslider_hp').bxSlider({
	          auto: true,
	          pause: 10000,
	          nextText: 'další',
	          prevText: 'předchozí',
	          pagerType: 'full',
	          pagerFullSeparator: '/'
	      });
	  }
	  if($( ".bxslider_sp" ).length > 0 ){
	      $('.bxslider_sp').bxSlider({
	          auto: true,
	          pause: 10000,
	          nextText: 'další',
	          prevText: 'předchozí',
	          pagerType: 'full',
	          pagerFullSeparator: '/',
	          slideWidth: 437
	      });
	  }
      $("#back-top").hide();
      $('#back-top').click(function(e){
          e.preventDefault();
       	  $('html, body').animate({scrollTop: 0}, 300);
          return false;
      });
      $("#progress-wrap").css({
        'top': ($('#content').outerHeight() / 2) - 100,
        'left': ($('#content').outerWidth() / 2) - 100,
        'display': 'block'
      });
      
      $("#progress-bar > span").each(function () {
            $(this).data("origWidth", $(this).width()).width(0).animate({
              width: $(this).data("origWidth")
            }, 1200);
      });
});

$(window).load(function () {
  	$('#content .container').css({
      	'opacity': 1
    });
  	$('#content').css({
      	'max-height': 'none'
    }).removeClass('overflow-hidden');
    $('#progress-bar').hide();
  $('#intro-description').delay(4000).animate({'bottom': '-1px'}, 300).delay(7000).animate({'bottom': '-37px'}, 300);
});
$(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        	$('#back-top').fadeIn();
      } else {
        	$('#back-top').fadeOut();
      }
});
$(window).resize(function(){
  	  var windowHeight = $('#header').outerHeight() + $('#content').outerHeight() +
          $('#footer').outerHeight() + $('#copyright').outerHeight();
  	  if ($(window).outerHeight() > windowHeight) {
        	$('#content .container').css('min-height', $('#content').outerHeight() + $(window).outerHeight() - windowHeight - 60);
      }
});

if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}