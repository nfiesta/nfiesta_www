<?php
/* 
------------------
Language: Czech
------------------
*/
$lang = array();
$lang['TOP_HEADER_COMPANY'] = 'Ústav pro hospodářskou úpravu lesů Brandýs nad Labem';
$lang['TOP_HEADER_COMPANY_LINK'] = 'www.uhul.cz';
$lang['HEADER_COMPANY_SHORT'] = 'NIL';
$lang['HEADER_COMPANY_LONG'] = 'Národní inventarizace lesů';
$lang['COPYRIGHT_DESCRIPTION'] = 'všechna práva vyhrazena';
$lang['MAP_PAGE_HREF'] = 'mapa-stranek';
$lang['MAP_PAGE_TITLE'] = 'Mapa stránek';
$lang['LINKS_HREF'] = 'odkazy';
$lang['LINKS_TITLE'] = 'Odkazy';
$lang['BACK_TOP'] = 'Nahoru';
$lang['PREVIOUS'] = 'Předchozí';
?>