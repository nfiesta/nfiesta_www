<?php
/* 
------------------
Language: English
------------------
*/
$lang = array();
$lang['TOP_HEADER_COMPANY'] = 'Ústav pro hospodářskou úpravu lesů Brandýs nad Labem';
$lang['TOP_HEADER_COMPANY_LINK'] = 'www.uhul.cz/home';
$lang['HEADER_COMPANY_SHORT'] = 'NFI';
$lang['HEADER_COMPANY_LONG'] = 'National Forest Inventory';
$lang['COPYRIGHT_DESCRIPTION'] = 'all rights reserved';
$lang['MAP_PAGE_HREF'] = 'en/sitemap';
$lang['MAP_PAGE_TITLE'] = 'Site map';
$lang['LINKS_HREF'] = 'en/links';
$lang['LINKS_TITLE'] = 'Links';
$lang['BACK_TOP'] = 'Up';
$lang['PREVIOUS'] = 'Previous';
?>