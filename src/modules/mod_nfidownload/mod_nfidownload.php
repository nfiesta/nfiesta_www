<?php
/**
 * NFI Download! Module Entry Point
 * 
 * @package    NFI.web
 * @subpackage Modules
 * @link http://nil.uhul.cz
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$jlang = JFactory::getLanguage();
$lang_nfidownload = $jlang->getTag();

$scheme = JFactory::getUri()->getScheme();
$hostname = JFactory::getUri()->getHost();

if($lang_nfidownload == 'en-GB') {
JFactory::getLanguage()->load('mod_nfidownload', JPATH_SITE, 'en-GB', true);
}
else {
JFactory::getLanguage()->load('mod_nfidownload', JPATH_SITE, 'cs-CZ', true);
}

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}
// Include the syndicate functions only once
require_once( dirname(__FILE__).'/file.php' );
require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'configuration.php' );
 
$config = new NFIConfig;

$jinput = JFactory::getApplication()-> input;

JFactory::getLanguage()->load('mod_nfidownload', JPATH_SITE, 'cs-CZ', true);

$table = modNfiDownload::getTableHTML($config->file_config['dir'],$config->file_config['types'],$jinput,$scheme,$hostname);
require( JModuleHelper::getLayoutPath( 'mod_nfidownload' ) );
 
?>