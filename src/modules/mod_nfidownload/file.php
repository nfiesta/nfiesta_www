<?php
/**
 * NFI Download! Module Entry Point
 * 
 * @package    NFI.web
 * @subpackage Modules
 * @link http://nil.uhul.cz
 */

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

class modNfiDownload
{
    /**
     * Retrieves the hello message
     *
     * @param array $params An object containing the module parameters
     * @access public
     */    
    public static function getTableHTML($dir,$types,$jinput,$scheme,$hostname)
    {  
    	$subdir = $jinput->get('subdir', 'vysledky_projektu_ssvle','str');
    	
    	$files = scandir($dir . DS . $subdir, 1);
    	
    	$rows = array();

        //----------------------------------------------------------------------
        if ($subdir == 'vysledky_projektu_nil2')
        {
            $files_add = scandir($dir, 1);
            
    	       foreach ($files_add as $file_add) {
    		      $filename_add = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file_add);
    		      $ext_add = pathinfo($file_add, PATHINFO_EXTENSION);
                    $filedescription_add = JText::_(strtoupper($filename_add));
               		
    		      if ($ext_add and strpos($types,$ext_add) !== false) {
    			     $rows[] = array($filename_add,$ext_add,$filedescription_add);
    		      }
    	       }            
        }
    	//----------------------------------------------------------------------
        // cyklus pro klasickou slozku -----------------------------------------    	
    	foreach ($files as $file) {
    		$filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file);
    		$ext = pathinfo($file, PATHINFO_EXTENSION);
            $filedescription = JText::_(strtoupper($filename));
               		
    		if ($ext and strpos($types,$ext) !== false) {
    			$rows[] = array($filename,$ext,$filedescription);
    		}
    	}
        //----------------------------------------------------------------------
    	
    	$htmlTable = "";
    	
    	foreach($rows as $row) {
    		$htmlTable .= "<tr>";
    		$htmlTable .= "<td>". $row[2] . "</td>";
            if ($row[0] == '2019_kniha_nil2_web')
            {
    		  $htmlTable .= "<td><a download target='_blank' href='". $scheme . "://" . $hostname . DS . "web/downloads" . DS . $row[0] . "." . $row[1] . "'>" . $row[1] . "</a></td>";
            }
            else
            {
    		  $htmlTable .= "<td><a download target='_blank' href='". $scheme . "://" . $hostname . DS . "web/downloads" . DS . $subdir . DS . $row[0] . "." . $row[1] . "'>" . $row[1] . "</a></td>";
            }
    		$htmlTable .= "</tr>";
    	}
    	
        return $htmlTable;
    }
    
}
?>
