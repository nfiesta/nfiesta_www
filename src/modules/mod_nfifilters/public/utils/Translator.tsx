/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { Signal, signal } from "@preact/signals";
import { createContext } from "preact";

import csCz from "../../language/cs-CZ/cs-CZ.mod_nfifilters.ini?raw";
import enGb from "../../language/en-GB/en-GB.mod_nfifilters.ini?raw";

export const TranslatorContext = createContext<Translator>(null as unknown as Translator);

type ITranslations = Record<string, Record<string, string> | string>;

export class Translator {

	private readonly translations: ITranslations = {};
	private _initialized = signal(false);

	public constructor(lang: string) {
		if (lang === "cs-CZ") {
			this.translations = this.parseIni(csCz);
		} else {
			this.translations = this.parseIni(enGb);
		}

		void this.init();
	}

	private async init() {
		const request = await fetch(
			window.baseUrl + "/index.php?option=com_nfidata&format=json&dataset=translations",
		);
		const data = await request.json() as {gui_header: string; label: string}[];

		if (!this.translations.hasOwnProperty("filters")) {
			this.translations.filters = {};
		}

		for (const translation of data) {
			(this.translations.filters as ITranslations)[this.convertName(translation.gui_header)] = translation.label;
		}

		this.initialized.value = true;
	}

	private parseIni(data: string): ITranslations {
		const paramRegex = /^\s*([^=]+?)\s*=\s*(.*?)\s*$/;
		const sectionRegex = /^\s*\[\s*([^\]]*)\s*]\s*$/;
		const commentRegex = /^\s*;.*$/;
		const value: ITranslations = {};
		let section: string | null = null;

		const lines = data.split(/[\r\n]+/);
		lines.forEach(line => {
			if (commentRegex.test(line)) {
				return;
			} else if (paramRegex.test(line)) {
				const match = line.match(paramRegex) as RegExpMatchArray;

				if (section) {
					(value[section] as ITranslations)[this.convertName(match[1])] = this.convertValue(match[2]);
				} else {
					value[this.convertName(match[1])] = this.convertValue(match[2]);
				}
			} else if (sectionRegex.test(line)) {
				const match = line.match(sectionRegex) as RegExpMatchArray;
				value[this.convertName(match[1])] = {};
				section = this.convertName(match[1]);
			} else if (line.length === 0 && section) {
				section = null;
			}
		});

		return value;
	}

	public convertName(name: string): string {
		return name.toLowerCase().replace(/(_\w)/g, k => k[1].toUpperCase());
	}

	private convertValue(value: string): string {
		return value.substring(1, value.length - 1);
	}

	public translate(value: string): string {
		const keys = value.split(".");
		let translations = this.translations;

		for (const key of keys) {
			if (!translations.hasOwnProperty(key)) {
				return value;
			}

			translations = translations[key] as Record<string, string>;
		}

		if (!translations || typeof(translations) !== "string") {
			return value;
		}

		return translations;
	}

	public translateCapitalizedFirstLetter(value: string): string {
		const translation = this.translate(value);

		return translation.charAt(0).toUpperCase() + translation.slice(1);
	}

	public get initialized(): Signal<boolean> {
		return this._initialized;
	}

}
