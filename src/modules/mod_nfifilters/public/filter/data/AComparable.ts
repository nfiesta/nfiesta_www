/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

export enum CompareStatus {
	SAME,
	CHANGED,
	ADDED,
	REMOVED,
}

export abstract class AComparable {

	private _compareStatus: CompareStatus = CompareStatus.SAME;

	public set isAdded(value : boolean) {
		this._compareStatus = value ? CompareStatus.ADDED : CompareStatus.SAME;
	}

	public get isAdded(): boolean {
		return this._compareStatus === CompareStatus.ADDED;
	}

	public set isRemoved(value : boolean) {
		this._compareStatus = value ? CompareStatus.REMOVED : CompareStatus.SAME;
	}

	public get isRemoved(): boolean {
		return this._compareStatus === CompareStatus.REMOVED;
	}

	public set isChanged(value : boolean) {
		this._compareStatus = value ? CompareStatus.CHANGED : CompareStatus.SAME;
	}

	public get isChanged(): boolean {
		return this._compareStatus === CompareStatus.CHANGED;
	}

	public abstract compare(compared: AComparable): number;

}
