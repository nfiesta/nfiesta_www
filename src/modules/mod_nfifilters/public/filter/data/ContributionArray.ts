/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {AComparable} from "./AComparable.ts";

export class ContributionArray<T> extends AComparable {

	public constructor(private items: T[]) {
		super();
	}

	public map<R>(mapper: (t: T) => R) {
		return this.items.map(mapper);
	}

	public sort(fce: (a:T, b: T) => number) {
		return this.items.sort(fce);
	}

	public push(value: T) {
		this.items.push(value);
	}

	public [Symbol.iterator]() {
		let index = -1;
		const data  = this.items;

		return {
			next: () => ({ value: data[++index], done: !(index in data) }),
		};
	};

	public compare(): number {
		throw new Error("Not implemented");
	}

}
