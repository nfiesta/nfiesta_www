/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */
import {Contribution} from "./Contribution.ts";


export interface IOptionData {
	id: string;
	label: string;
	group?: string;
	domain?: string;
	detailed: boolean;
}

export interface IContributionOptionData {
	label: string;
	description: string;
}

export interface IRestrictionData {
	object: IContributionOptionData;
	restriction: IContributionOptionData;
}

export interface IContributionData {
	area_domain_restrictions: IRestrictionData[];
	definition_variant: IContributionOptionData[];
	ldsity: IContributionOptionData;
	ldsity_object: IContributionOptionData;
	ldsity_type: IContributionOptionData;
	sub_population_restrictions: IRestrictionData[];
	use_negative: "false" | "true" | null;
	version: IContributionOptionData;
}

export class Option {

	private readonly _id: string;
	private readonly _label: string;
	private readonly _domain: string | null;
	private readonly _detailed: boolean;

	public constructor(obj: IOptionData) {
		this._id = obj.group ? obj.group : obj.id;
		this._label = obj.label;
		this._detailed = obj.detailed;
		this._domain = obj.domain ?? null;
	}

	public get id(): string {
		return this._id;
	}

	public get label(): string {
		return this._label;
	}

	public get domain(): string | null {
		return this._domain;
	}

	public get detailed(): boolean {
		return this._detailed;
	}

	public get contribution(): Contribution[] {
		const jsonData = JSON.parse(this.label) as IContributionData[];

		return jsonData.map(data => new Contribution(data));
	}

}
