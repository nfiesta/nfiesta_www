/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {AComparable} from "./AComparable.ts";
import {ContributionArray} from "./ContributionArray.ts";
import {ContributionOption} from "./ContributionOption.ts";
import {ContributionRestriction} from "./ContributionRestriction.ts";
import {IContributionData} from "./Option.ts";

export class Contribution extends AComparable {

	private _areaDomainRestrictions: ContributionArray<ContributionRestriction>;
	private _definitionVariant: ContributionArray<ContributionOption>;
	private _ldsity: ContributionOption;
	private _ldsityObject: ContributionArray<ContributionOption>;
	private _ldsityType: ContributionArray<ContributionOption>;
	private _subPopulationRestrictions: ContributionArray<ContributionRestriction>;
	private _useNegative: boolean | null;
	private _version: ContributionArray<ContributionOption>;


	public constructor(data: IContributionData) {
		super();

		this._areaDomainRestrictions = new ContributionArray(
			data.area_domain_restrictions.map(v => new ContributionRestriction(v)),
		);
		this._definitionVariant = new ContributionArray(data.definition_variant.map(v => new ContributionOption(v)));
		this._ldsity = new ContributionOption(data.ldsity);
		this._ldsityObject = new ContributionArray([new ContributionOption(data.ldsity_object)]);
		this._ldsityType = new ContributionArray([new ContributionOption(data.ldsity_type)]);
		this._subPopulationRestrictions = new ContributionArray(
			data.sub_population_restrictions.map(v => new ContributionRestriction(v)),
		);
		this._useNegative = data.use_negative === "true" ? true : (data.use_negative === "false" ? false : null);
		this._version = new ContributionArray([new ContributionOption(data.version)]);
	}

	public get areaDomainRestrictions(): ContributionArray<ContributionRestriction> {
		return this._areaDomainRestrictions;
	}

	public get definitionVariant(): ContributionArray<ContributionOption> {
		return this._definitionVariant;
	}

	public get ldsity(): ContributionOption {
		return this._ldsity;
	}

	public get ldsityObject(): ContributionArray<ContributionOption> {
		return this._ldsityObject;
	}

	public get ldsityType(): ContributionArray<ContributionOption> {
		return this._ldsityType;
	}

	public get subPopulationRestrictions(): ContributionArray<ContributionRestriction> {
		return this._subPopulationRestrictions;
	}

	public get useNegative(): boolean | null {
		return this._useNegative;
	}

	public get version(): ContributionArray<ContributionOption> {
		return this._version;
	}

	public clone(): Contribution {
		const cloned = Object.assign(
			Object.create(Object.getPrototypeOf(this) as Contribution),
			this as Contribution,
		) as Contribution;
		cloned._areaDomainRestrictions = new ContributionArray([...this._areaDomainRestrictions].map(v => v.clone()));
		cloned._definitionVariant = new ContributionArray([...this._definitionVariant].map(dv => dv.clone()));
		cloned._ldsity = this._ldsity.clone();
		cloned._ldsityObject = new ContributionArray([...this._ldsityObject].map(o => o.clone()));
		cloned._ldsityType = new ContributionArray([...this._ldsityType].map(t => t.clone()));
		cloned._subPopulationRestrictions = new ContributionArray(
			[...this._subPopulationRestrictions].map(spr => spr.clone()),
		);
		cloned._version = new ContributionArray([...this._version].map(v => v.clone()));

		return cloned;
	}

	public compare(compared: Contribution): number {
		return (this.useNegative === compared.useNegative && this.ldsity.label === compared.ldsity.label) ? 0 : 1;
	}

}
