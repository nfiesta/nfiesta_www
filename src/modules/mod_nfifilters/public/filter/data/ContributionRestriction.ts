/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {AComparable} from "./AComparable.ts";
import {ContributionOption} from "./ContributionOption.ts";
import {IRestrictionData} from "./Option.ts";

export class ContributionRestriction extends AComparable {

	private _object: ContributionOption;
	private _restriction: ContributionOption;

	public constructor(data: IRestrictionData) {
		super();

		this._object = new ContributionOption(data.object);
		this._restriction = new ContributionOption(data.restriction);
	}

	public get object(): ContributionOption {
		return this._object;
	}

	public get restriction(): ContributionOption {
		return this._restriction;
	}

	public clone(): ContributionRestriction {
		const cloned = Object.assign(
			Object.create(Object.getPrototypeOf(this) as ContributionRestriction),
			this as ContributionRestriction,
		) as ContributionRestriction;
		cloned._object = this._object.clone();
		cloned._restriction = this._restriction.clone();

		return cloned;
	}

	public compare(compared: ContributionRestriction): number {
		const labelCompare = this.object.compare(compared.object);
		if (labelCompare !== 0) {
			return labelCompare;
		}

		return this.restriction.compare(compared.restriction);
	}

}
