/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {AComparable} from "./AComparable.ts";
import {IContributionOptionData} from "./Option.ts";

export class ContributionOption extends AComparable {

	private readonly _label: string;
	private readonly _description: string;

	public constructor(data: IContributionOptionData) {
		super();

		this._label = data.label;
		this._description = data.description;
	}

	public get label(): string {
		return this._label;
	}

	public get description(): string {
		return this._description;
	}

	public clone(): ContributionOption {
		return Object.assign(
			Object.create(Object.getPrototypeOf(this) as ContributionOption),
			this as ContributionOption,
		) as ContributionOption;
	}

	public compare(compared: ContributionOption): number {
		return this.label.localeCompare(compared.label);
	}

}
