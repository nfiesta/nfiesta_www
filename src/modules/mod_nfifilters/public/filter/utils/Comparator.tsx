/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {AComparable} from "../data/AComparable.ts";
import {Contribution} from "../data/Contribution.ts";
import {ContributionArray} from "../data/ContributionArray.ts";

export class Comparator {

	public static compare(original: Contribution[], compared: Contribution[] | null): Contribution[] {
		if (!compared) {
			return original;
		}

		const contributionsToPair = [...compared].map(c => c.clone());
		const result = [...original].map(c => c.clone());
		for (const row of result) {
			const pairIdx = contributionsToPair.findIndex(contributionPair => row.compare(contributionPair) === 0);
			if (pairIdx !== -1) {
				this.compareObject(row, contributionsToPair[pairIdx]);
				contributionsToPair.splice(pairIdx, 1);
			} else {
				row.isAdded = true;
			}
		}

		for (const row of contributionsToPair) {
			row.isRemoved = true;
			result.push(row);
		}

		return result;
	}

	private static compareObject(original: Contribution, compared: Contribution) {
		if (!compared) {
			return;
		}

		const properties = Object.entries(Object.getOwnPropertyDescriptors(Object.getPrototypeOf(original)))
			.filter(e => typeof e[1].get === "function" && e[0] !== "__proto__")
			.map(e => e[0])
			.filter(n => n !== "ldsity" && n !== "useNegative")
		;

		properties.forEach(property => {
			const value = (original as unknown as {[k: string]: ContributionArray<AComparable>})[property];
			const comparedValue = (compared as unknown as {[k: string]: ContributionArray<AComparable>})[property];

			if (value instanceof ContributionArray) {
				if (this.compareArray(value, comparedValue)) {
					value.isChanged = true;
				}
			} else {
				throw new Error("Unknown value");
			}
		});
	}

	private static compareArray<T extends AComparable>(
		original: ContributionArray<T>,
		compared: ContributionArray<T>,
	): boolean {
		let isChanged = false;
		const comparedItems = [...compared];

		for (const value of original) {
			const comparedValueIdx = comparedItems.findIndex(v => v.compare(value) === 0);

			if (comparedValueIdx === -1) {
				value.isAdded = true;
				isChanged = true;

				continue;
			}

			comparedItems.splice(comparedValueIdx, 1);
		}

		for (const compareValue of comparedItems) {
			isChanged = true;
			compareValue.isRemoved = true;
			original.push(compareValue);
		}

		original.sort((a, b) => a.compare(b));

		return isChanged;
	}

}
