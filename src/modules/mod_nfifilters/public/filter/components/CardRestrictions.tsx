/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { useContext } from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {ContributionArray} from "../data/ContributionArray.ts";
import {ContributionRestriction} from "../data/ContributionRestriction.ts";

import {CardRestriction} from "./CardRestriction.tsx";

export const CardRestrictions = ({
	restrictions,
	collapsed,
	title,
}: {
	restrictions: ContributionArray<ContributionRestriction>;
	collapsed: boolean;
	title: string;
}) => {
	if (collapsed && !restrictions.isChanged) {
		return null;
	}

	const translator = useContext(TranslatorContext);
	return <div className="filter__card__item">
		<b>{title}:</b>
		<small>({translator.translate("filters.object")} / {translator.translate("filters.restriction")})</small>
		<div className="filter__card__value filter__card__value--full">
			{restrictions.map(restriction => <CardRestriction restriction={restriction} collapsed={collapsed}/>)}
		</div>
	</div>;
};

