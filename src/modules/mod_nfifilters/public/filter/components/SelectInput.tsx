/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { useContext } from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {BaseFilter} from "../filters/BaseFilter.ts";

export const SelectInput = ({entry}: {entry: BaseFilter}) => {
	const translator = useContext(TranslatorContext);

	const handleSelect = (e: Event) => {
		const option = entry.values.value.find(o => o.id === (e.target as HTMLSelectElement).value);
		void entry.setValue(option ?? null);
	};

	if (entry.disabled.value) {
		return null;
	}

	return <div className="filter__select">
		<h4>{translator.translateCapitalizedFirstLetter("filters." + entry.name)}</h4>
		<select
			name={entry.name}
			disabled={entry.values.value.length < 2}
			value={entry.value?.value?.id ?? ""}
			onChange={handleSelect}
		>
			{entry.values.value.length > 1 && <option disabled value="">...</option>}
			{entry.values.value.map(option => <option value={option.id}>{option.label}</option>)}
		</select>
	</div>;

};
