/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { useContext } from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {Contribution} from "../data/Contribution.ts";

import {CardOptions} from "./CardOptions.tsx";
import {CardRestrictions} from "./CardRestrictions.tsx";

export const CardSegment = ({
	contribution,
	collapsed,
	handleCollapse,
}: {
	contribution: Contribution;
	collapsed: boolean;
	handleCollapse?: (value: boolean) => void;
}) => {
	const translator = useContext(TranslatorContext);

	const onCollapse = (e: MouseEvent, collapse: boolean) => {
		e.preventDefault(); e.stopPropagation();
		if (handleCollapse) {
			handleCollapse(collapse);
		}
	};

	return <div className={
		"filter__card__segment"
		+ (contribution.isAdded ? " filter__card__segment--add" : "")
		+ (contribution.isRemoved ? " filter__card__segment--remove" : "")
	}>
		<div className="filter__card__heading">
			{contribution.useNegative === true &&
				<span className="filter__card__negative"/>}
			{contribution.useNegative === false &&
				<span className="filter__card__positive"/>}
			{contribution.ldsity.label}
		</div>

		{contribution.isAdded && <span className="badge" title={translator.translate("card.diffItemIns")}/>}
		{contribution.isRemoved && <span className="badge" title={translator.translate("card.diffItemDel")}/>}

		<CardOptions
			options={contribution.ldsityObject}
			collapsed={collapsed}
			title={translator.translate("filters.ldsityObject")}
		/>
		<CardOptions
			options={contribution.ldsityType}
			collapsed={collapsed}
			title={translator.translate("filters.ldsityType")}
		/>
		<CardOptions
			options={contribution.version}
			collapsed={collapsed}
			title={translator.translate("filters.version")}
		/>
		<CardOptions
			options={contribution.definitionVariant}
			collapsed={collapsed}
			title={translator.translate("filters.definitionVariant")}
		/>
		<CardRestrictions
			restrictions={contribution.areaDomainRestrictions}
			collapsed={collapsed}
			title={translator.translate("filters.areaDomainRestrictions")}
		/>
		<CardRestrictions
			restrictions={contribution.subPopulationRestrictions}
			collapsed={collapsed}
			title={translator.translate("filters.subPopulationRestrictions")}
		/>

		{collapsed && handleCollapse && <a
			onClick={e => onCollapse(e, false)}
			className="filter__card__toggle filter__card__toggle--open"
			title={translator.translate("buttons.expand")}
		>
			{translator.translate("buttons.expand")}
		</a>}

		{!collapsed && handleCollapse && <a
			onClick={e => onCollapse(e, true)}
			className="filter__card__toggle filter__card__toggle--close"
			title={translator.translate("buttons.collapse")}
		>
			{translator.translate("buttons.collapse")}
		</a>}
	</div>;
};
