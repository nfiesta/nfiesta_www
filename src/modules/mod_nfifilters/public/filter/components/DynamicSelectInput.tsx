/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {DynamicFilter} from "../filters/DynamicFilter.ts";

import {SelectInput} from "./SelectInput.tsx";

export const DynamicSelectInput = ({entry}: {entry: DynamicFilter}) => <>
	{entry.entries.value.map(e => <SelectInput entry={e} />)}
</>;
