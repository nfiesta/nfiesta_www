/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {ContributionArray} from "../data/ContributionArray.ts";
import {ContributionOption} from "../data/ContributionOption.ts";

import {CardOption} from "./CardOption.tsx";

export const CardOptions = ({
	options,
	collapsed,
	title,
}: {
	options: ContributionArray<ContributionOption>;
	collapsed: boolean;
	title: string;
}) => {
	if (collapsed && !options.isChanged) {
		return null;
	}

	return <div className="filter__card__item">
		<b>{title}:</b>
		<div className="filter__card__value">
			{options.map(o => <CardOption option={o} collapsed={collapsed}/>)}
		</div>
	</div>;
};


