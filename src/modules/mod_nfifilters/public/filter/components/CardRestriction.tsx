/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { useContext } from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {ContributionRestriction} from "../data/ContributionRestriction.ts";

export const CardRestriction = ({
	restriction,
	collapsed,
}: {
	restriction: ContributionRestriction;
	collapsed: boolean;
}) => {
	const translator = useContext(TranslatorContext);
	const content = <div>{restriction.object.label} / {restriction.restriction.label}</div>;

	if (restriction.isAdded) {
		return <ins>{content} <span className="badge" title={translator.translate("card.diffItemIns")}/>
		</ins>;
	}

	if (restriction.isRemoved) {
		return <del>{content} <span className="badge" title={translator.translate("card.diffItemDel")}/></del>;
	}

	if (collapsed) {
		return null;
	}

	return <>{content}</>;
};
