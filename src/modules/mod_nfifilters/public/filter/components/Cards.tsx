/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { useContext, useState } from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {Option} from "../data/Option.ts";
import {BaseFilter} from "../filters/BaseFilter.ts";

import {Card} from "./Card.tsx";

export const Cards = ({entry}: {entry: BaseFilter}) => {
	const translator = useContext(TranslatorContext);
	const [collapsed, setCollapsed] = useState(true);

	const handleSelect = (value: Option) => {
		void entry.setValue(value);
	};

	if (entry.disabled.value) {
		return null;
	}

	return <div className={"filter__cards" + (entry.values.value.length === 0 ? " filter__cards--empty" : "")}>
		<h4>{translator.translate("filters." + entry.name)}</h4>
		<div className="filter__cards__wrapper">
			{entry.values.value.map(option => <Card
				option={option}
				handleSelect={() => handleSelect(option)}
				selected={option === entry.value.value}
				compare={entry.value.value}
				collapsed={collapsed}
				setCollapsed={setCollapsed}
			/>)}
		</div>
	</div>;
};
