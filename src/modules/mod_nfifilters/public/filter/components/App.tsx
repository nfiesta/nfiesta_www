/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { useContext, useState } from "preact/hooks";

import {Translator, TranslatorContext} from "../../utils/Translator.tsx";
import {BaseFilter} from "../filters/BaseFilter.ts";
import {getContext} from "../filters/Context.ts";

import {Cards} from "./Cards.tsx";
import {DynamicSelectInput} from "./DynamicSelectInput.tsx";
import {SelectInput} from "./SelectInput.tsx";

const ResultButton = ({entry}: {entry: BaseFilter}) => {
	const translator = useContext(TranslatorContext);
	const url = window.baseLangUrl + "/tabulka?group=" + entry.value.value?.id;

	return <a href={url} className="filter__button filter__button--forward">
		{translator.translate("buttons.showResults")}
	</a>;
};

export const App = ({lang}: {lang: string}) => {
	const [dataContext] = useState(() => getContext());
	const [translator] = useState(() => new Translator(lang));
	const [detailed, setDetailed] = useState(false);

	if (!translator.initialized.value) {
		return <>{translator.translate("buttons.loading")}</>;
	}

	return <TranslatorContext.Provider value={translator}>
		<div className="nfiFilters">
			{!detailed && <div className="filter">
				<SelectInput entry={dataContext.numerator.topic}/>
				<SelectInput entry={dataContext.numerator.period}/>
				<SelectInput entry={dataContext.numerator.geographicDomain}/>
				<SelectInput entry={dataContext.numerator.indicator}/>
				<SelectInput entry={dataContext.numerator.stateOrChange}/>
				<SelectInput entry={dataContext.numerator.unit}/>
				<Cards entry={dataContext.numerator.localDensityContributionsCore}/>
				<DynamicSelectInput entry={dataContext.numerator.areaDomain}/>
				<DynamicSelectInput entry={dataContext.numerator.subPopulation}/>
				<Cards entry={dataContext.numerator.localDensityContributionsDivision}/>
				<SelectInput entry={dataContext.numerator.unitOfMeasurement}/>

				{!dataContext.numerator.unitOfMeasurement.value.value &&
					<a className="filter__button filter__button--disabled filter__button--forward">
						{translator.translate("buttons.showDetailed")}
					</a>
				}
				{!!dataContext.numerator.unitOfMeasurement.value.value
					&& !dataContext.denominator.indicator.disabled.value &&
					<a className="filter__button filter__button--forward" onClick={() => setDetailed(true)}>
						{translator.translate("buttons.showDetailed")}
					</a>
				}
				{!!dataContext.numerator.unitOfMeasurement.value.value
					&& dataContext.denominator.indicator.disabled.value &&
					<ResultButton entry={dataContext.numerator.unitOfMeasurement} />
				}
			</div>}

			{detailed && <a onClick={() => setDetailed(false)} className="filter__button filter__button--back">
				{translator.translate("buttons.back")}
			</a>}

			{detailed && <div className="filter">
				<SelectInput entry={dataContext.denominator.indicator} />
				<SelectInput entry={dataContext.denominator.stateOrChange} />
				<Cards entry={dataContext.denominator.localDensityContributionsCore} />
				<DynamicSelectInput entry={dataContext.denominator.areaDomain} />
				<DynamicSelectInput entry={dataContext.denominator.subPopulation} />
				<Cards entry={dataContext.denominator.localDensityContributionsDivision} />

				{!!dataContext.denominator.localDensityContributionsDivision.value.value && <ResultButton
					entry={dataContext.denominator.localDensityContributionsDivision}
				/>}
			</div>}
		</div>
	</TranslatorContext.Provider>;
};
