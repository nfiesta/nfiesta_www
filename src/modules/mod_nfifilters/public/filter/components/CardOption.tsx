/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {useContext} from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {ContributionOption} from "../data/ContributionOption.ts";

export const CardOption = ({option, collapsed}: {option: ContributionOption; collapsed: boolean}) => {
	const translator = useContext(TranslatorContext);

	if (option.isAdded) {
		return <ins>{option.label} <span className="badge" title={translator.translate("card.diffItemIns")}/></ins>;
	}

	if (option.isRemoved) {
		return <del>{option.label} <span className="badge" title={translator.translate("card.diffItemDel")} /></del>;
	}

	if (collapsed) {
		return null;
	}

	return <div>{option.label}</div>;
};
