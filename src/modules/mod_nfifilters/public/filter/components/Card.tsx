/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {Option} from "../data/Option.ts";
import {Comparator} from "../utils/Comparator.tsx";

import {CardSegment} from "./CardSegment.tsx";

export const Card = ({
	option,
	handleSelect,
	selected,
	collapsed,
	setCollapsed,
	compare,
}: {
	option: Option;
	handleSelect?: () => void;
	selected: boolean;
	collapsed: boolean;
	setCollapsed?: (v: boolean) => void;
	compare?: Option | null;
}) => {
	const contributions = Comparator.compare(
		option.contribution,
		compare ? compare.contribution : null
	);

	return <div className={"filter__card" + (selected ? " filter__card--selected" : "")} onClick={handleSelect}>
		{contributions.map(row => <CardSegment
			contribution={row}
			collapsed={collapsed}
			handleCollapse={setCollapsed}
		/>)}
	</div>;
};

