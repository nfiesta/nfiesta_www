/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {BaseFilter} from "./BaseFilter.ts";

type ConditionalDataEntryConstructor = new <T>(filter: T, stopPropagation?: boolean) => T;

class ConditionalDataEntryImpl<T extends BaseFilter> {

	public constructor(private filter: T, private stopPropagation = false) {
		return this.createProxy();
	}

	public get(target: ConditionalDataEntryImpl<T>, prop: keyof T | symbol | string) {
		return target.filter[prop as keyof T];
	}

	public set(target: ConditionalDataEntryImpl<T>, prop: keyof T | symbol | string, value: T[keyof T]): boolean {
		target.filter[prop as keyof T] = value;
		return true;
	}

	public createProxy(): ConditionalDataEntryImpl<T> {
		const originalInit = this.filter.init; // eslint-disable-line @typescript-eslint/unbound-method
		this.filter.init = async () => {
			if (this.filter.ancestor?.value.value?.detailed) {
				this.filter.disabled.value = false;
				await originalInit.call(this.filter);
			} else {
				this.filter.disabled.value = true;

				if (!this.stopPropagation) {
					await this.filter.setValue(this.filter.ancestor?.value.value ?? null);
				}
			}
		};

		return new Proxy(this, this);
	}

}

export const ConditionalFilter = ConditionalDataEntryImpl as ConditionalDataEntryConstructor;
