/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { Signal, effect, signal } from "@preact/signals";

import {BaseFilter} from "./BaseFilter.ts";
import {GroupFilter} from "./GroupFilter.ts";

export class DynamicFilter extends BaseFilter {

	protected _entries: Signal<GroupFilter[]> = signal([]);

	public constructor(name: string, dataset: string, ancestor?: BaseFilter) {
		super(name, dataset, ancestor);

		this._entries.value = [];
		this.createEntry();
	}

	public get entries(): Signal<GroupFilter[]> {
		return this._entries;
	}

	public async init() {
		this._values.value = [];
		this._entries.value = [];
		const entry = this.createEntry();
		await this.initSub(entry);
	}

	private async initSub(entry: GroupFilter) {
		let data = null;
		const idx = this._entries.value.indexOf(entry);
		if (idx === 0) {
			data = this._ancestor?.getData();
		} else {
			data = this._entries.value[idx - 1].getData();
		}

		const options = await this.fetchData(this._dataset, data);
		await entry.setValues(options);
	}

	public async reset() {
		await super.reset();
		this._entries.value = [];
		this.createEntry();
	}

	public getData(): Record<string, string | undefined> {
		return {
			group: this.value.peek()?.id ?? "",
		};
	}

	public set descendant(value: BaseFilter | null) {
		if (value && this._entries.value.includes(value)) {
			return;
		}

		super.descendant = value;
	}

	private createEntry(): GroupFilter {
		const entry = new GroupFilter(this._name, this._dataset);
		effect(() => this.selectedValue(entry));
		this._entries.value = [...this._entries.value, entry];

		return entry;
	}

	private async selectedValue(entry: GroupFilter) {
		if (!entry.value.value) {
			return;
		}

		const entries = this._entries.peek();
		const idx = entries.indexOf(entry);
		if (idx === -1) {
			return;
		}

		entries.splice(idx + 1);
		this._entries.value = [...this._entries.peek()];

		const data = entries[idx].getData();
		const options = await this.fetchData(this._dataset, data);
		if (options.length === 0 || options.length === 1 && !options[0].label) {
			await this.setValue(entry.value.value);

			return;
		} else {
			await this.setValue(null);
		}

		const nextEntry = this.createEntry();
		await nextEntry.setValues(options);
	}

}
