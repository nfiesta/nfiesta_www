/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {ConditionalFilter} from "./ConditionalFilter.ts";
import {DynamicFilter} from "./DynamicFilter.ts";
import {FilledFilter} from "./FilledFilter.ts";
import {GroupFilter} from "./GroupFilter.ts";
import {ValueFilter} from "./ValueFilter.ts";

const topicNumerator = new ValueFilter(
	"topic",
	"basic-numerator",
);
const periodNumerator = new ValueFilter(
	"period",
	"basic-numerator",
	topicNumerator,
);
const geographicDomainNumerator = new ValueFilter(
	"geographicRegion",
	"basic-numerator",
	periodNumerator,
);
const indicatorNumerator = new FilledFilter(
	"indicator",
	"basic-numerator",
	geographicDomainNumerator,
);
const stateOrChangeNumerator = new FilledFilter(
	"stateOrChange",
	"basic-numerator",
	indicatorNumerator,
);
const unitNumerator = new GroupFilter(
	"unit",
	"basic-numerator",
	stateOrChangeNumerator,
);
const localDensityContributionsCoreNumerator = new GroupFilter(
	"coreContributions",
	"local-density-core-numerator",
	unitNumerator,
);
const areaDomainNumerator = new DynamicFilter(
	"areaDomain",
	"area-domain-numerator",
	localDensityContributionsCoreNumerator,
);
const subPopulationNumerator = new DynamicFilter(
	"subPopulationAttribute",
	"sub-population-numerator",
	areaDomainNumerator,
);
const localDensityContributionsDivisionNumerator = new ConditionalFilter(new GroupFilter(
	"divisionContributions",
	"local-density-division-numerator",
	subPopulationNumerator,
));
const unitOfMeasurementNumerator = new GroupFilter(
	"unitOfMeasurement",
	"unit-of-measurement",
	localDensityContributionsDivisionNumerator,
);
const indicatorDenominator = new ConditionalFilter(new FilledFilter(
	"indicator",
	"basic-denominator",
	unitOfMeasurementNumerator,
), true);
const stateOrChangeDenominator = new FilledFilter(
	"stateOrChange",
	"basic-denominator",
	indicatorDenominator,
);
const localDensityContributionsCoreDenominator = new GroupFilter(
	"coreContributions",
	"local-density-core-denominator",
	stateOrChangeDenominator,
);
const areaDomainDenominator = new DynamicFilter(
	"areaDomain",
	"area-domain-denominator",
	localDensityContributionsCoreDenominator,
);
const subPopulationDenominator = new DynamicFilter(
	"subPopulationAttribute",
	"sub-population-denominator",
	areaDomainDenominator,
);
const localDensityContributionsDivisionDenominator = new ConditionalFilter(new GroupFilter(
	"divisionContributions",
	"local-density-division-denominator",
	subPopulationDenominator,
));

export const getContext = () => {
	void topicNumerator.init();

	return {
		numerator: {
			topic: topicNumerator,
			period: periodNumerator,
			geographicDomain: geographicDomainNumerator,
			indicator: indicatorNumerator,
			stateOrChange: stateOrChangeNumerator,
			unit: unitNumerator,
			localDensityContributionsCore: localDensityContributionsCoreNumerator,
			areaDomain: areaDomainNumerator,
			subPopulation: subPopulationNumerator,
			localDensityContributionsDivision: localDensityContributionsDivisionNumerator,
			unitOfMeasurement: unitOfMeasurementNumerator,
		},
		denominator: {
			indicator: indicatorDenominator,
			stateOrChange: stateOrChangeDenominator,
			localDensityContributionsCore: localDensityContributionsCoreDenominator,
			areaDomain: areaDomainDenominator,
			subPopulation: subPopulationDenominator,
			localDensityContributionsDivision: localDensityContributionsDivisionDenominator,
		},
	};
};
