/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {BaseFilter} from "./BaseFilter.ts";

export class GroupFilter extends BaseFilter {

	public getData(): Record<string, string | undefined> {
		const data = {
			group: this.value.peek()?.id,
			domain: this.value.peek()?.domain ?? undefined,
		};

		if (!data.domain) {
			delete data.domain;
		}

		return data;
	}

}
