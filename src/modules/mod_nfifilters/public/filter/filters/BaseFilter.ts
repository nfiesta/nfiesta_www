/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { Signal, batch, signal } from "@preact/signals";

import {IOptionData, Option} from "../data/Option.ts";

export abstract class BaseFilter {

	protected _disabled: Signal<boolean> = signal(false);
	protected _value: Signal<Option | null> = signal(null);
	protected _values: Signal<Option[]> = signal([]);

	protected _descendant: BaseFilter | null = null;
	protected _ancestor: BaseFilter | null = null;

	public constructor(
		protected _name: string,
		protected _dataset: string,
		ancestor: BaseFilter | null = null,
	) {
		this.ancestor = ancestor;
	}

	public get name(): string {
		return this._name;
	}

	public get value(): Signal<Option | null> {
		return this._value;
	}

	public async setValue(value: Option | null): Promise<void> {
		if (value === this._value.peek()) {
			return;
		}

		this._value.value = value;

		await this._descendant?.reset();

		if (value) {
			await this._descendant?.init();
		}
	}

	public get values(): Signal<Option[]> {
		return this._values;
	}

	public async setValues(values: Option[]): Promise<void> {
		await batch(async () => {
			this._values.value = values;

			if (values.length === 1) {
				await this.setValue(values[0]);
			}
		});
	}

	public get ancestor(): BaseFilter | null {
		return this._ancestor;
	}

	public get descendant(): BaseFilter | null {
		return this._descendant;
	}

	public set ancestor(value: BaseFilter | null) {
		if (value) {
			value.descendant = this;
		}

		this._ancestor = value;
	}

	public set descendant(value: BaseFilter | null) {
		if (this._descendant) {
			throw new Error("Descendant is already set.");
		}

		this._descendant = value;
	}

	public get disabled(): Signal<boolean> {
		return this._disabled;
	}

	public async init() {
		if (this._disabled.peek()) {
			return;
		}

		this._values.value = [];

		const params = this._ancestor ? this._ancestor.getData() : undefined;
		await this.setValues(await this.fetchData(this._dataset, params));
	}

	protected async fetchData(
		dataSet: string,
		params: Record<string, string | undefined> | undefined,
	): Promise<Option[]> {
		const filteredParams = params
			? Object.fromEntries(
				Object.entries(params)
					.filter(([_, value]) => value !== undefined)
			) as Record<string, string>
			: undefined;

		const request = await fetch(
			window.baseUrl + "/index.php?option=com_nfidata&format=json&dataset="
			+ dataSet
			+ "&"
			+ (new URLSearchParams(filteredParams)).toString(),
		);
		const data = await request.json() as IOptionData[];

		return data.map(d => new Option(d));
	}

	public async reset() {
		await this.setValue(null);
		this._values.value = [];
		this._disabled.value = false;
	}

	public abstract getData(): Record<string, string | undefined>;

}
