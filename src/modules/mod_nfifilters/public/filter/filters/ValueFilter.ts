/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {BaseFilter} from "./BaseFilter.ts";

export class ValueFilter extends BaseFilter {

	public getData(): Record<string, string | undefined> {
		return {
			...(this._ancestor ? this._ancestor.getData() : {}),
			[this._name]: this.value.peek()?.id,
		};
	}

}
