/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {BaseFilter} from "./BaseFilter.ts";

export class FilledFilter extends BaseFilter {

	public getData(): Record<string, string | undefined> {
		return {
			...(this._ancestor ? this._ancestor.getData() : {}),
			group: this.value.peek()?.id,
			[this._name]: !!this.value ? "1" : "0",
		};
	}

}
