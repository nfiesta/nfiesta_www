/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import { render } from "preact";

import {App} from "./filter/components/App.tsx";
import {Metadata} from "./results/components/Metadata.tsx";

import "./assets/styles/app.scss"; // eslint-disable-line import/no-unassigned-import


const filterElement = document.querySelector(".nfi-filters") as HTMLDivElement;
const metadataElement = document.querySelector(".metadata") as HTMLDivElement;
window.baseUrl = filterElement?.dataset.baseUrl ?? metadataElement?.dataset.baseUrl ?? "";
window.baseLangUrl = filterElement?.dataset.baseLangUrl ?? metadataElement?.dataset.baseLangUrl ?? "";

if (filterElement) {
	render(<App lang={filterElement.dataset.lang ?? ""} />, filterElement);
} else if (metadataElement) {
	render(<Metadata lang={metadataElement.dataset.lang ?? ""} />, metadataElement);
}
