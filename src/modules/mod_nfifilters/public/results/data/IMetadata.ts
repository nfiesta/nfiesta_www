/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

export interface IValue {
	description: string;
}

export interface IData {
	[k: string]: IValue | IData[];
}
