/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {useContext} from "preact/hooks";

import {TranslatorContext} from "../../utils/Translator.tsx";
import {IData, IValue} from "../data/IMetadata.ts";

import {EntryArray} from "./EntryArray.tsx";
import {EntryValue} from "./EntryValue.tsx";

export const Entries = ({data}: {data: IData}) => {
	const translator = useContext(TranslatorContext);

	return <table>
		{Object.entries(data).map(([key, value]) => {
			if (value.hasOwnProperty("label")) {
				return <EntryValue
					label={translator.translateCapitalizedFirstLetter("filters." + translator.convertName(key))}
					value={value as IValue}
				/>;
			} else if (Array.isArray(value)) {
				return <EntryArray label={key} value={value} />;
			} else {
				throw new Error("Unknown structure for key " + key);
			}
		})}
	</table>;
};
