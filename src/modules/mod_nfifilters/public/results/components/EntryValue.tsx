/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {IValue} from "../data/IMetadata.ts";

export const EntryValue = ({label, value}: {label: string; value: IValue}) => <tr>
	<th>{label}: </th>
	<td>{value.description}</td>
</tr>;
