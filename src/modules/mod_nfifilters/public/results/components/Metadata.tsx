/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {useEffect, useState} from "preact/hooks";

import {Translator, TranslatorContext} from "../../utils/Translator.tsx";
import {IData} from "../data/IMetadata.ts";

import {Entries} from "./Entries.tsx";

export const Metadata = ({lang}: {lang: string}) => {
	const [translator] = useState(() => new Translator(lang));
	const [data, setData] = useState<IData | null>(null);

	useEffect(() => {
		const fetchData = async () => {
			const request = await fetch(
				window.baseUrl + "/index.php?option=com_nfidata&format=json&dataset=metadata&group="
				+ new URLSearchParams(window.location.search).get("group"),
			);

			setData(await request.json() as IData);
		};

		void fetchData();
	}, []);

	if (!translator.initialized.value || !data) {
		return <>{translator.translate("buttons.loading")}</>;
	}

	return <TranslatorContext.Provider value={translator}>
		{data && <Entries data={data} />}
	</TranslatorContext.Provider>;
};
