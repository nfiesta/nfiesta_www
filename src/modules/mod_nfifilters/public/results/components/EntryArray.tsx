/**
 * Author: Ondřej Václavík
 * Email: ahoj@ondrejvaclavik.cz
 */

import {useContext, useState} from "preact/hooks";

import {Card} from "../../filter/components/Card.tsx";
import {Option} from "../../filter/data/Option.ts";
import {TranslatorContext} from "../../utils/Translator.tsx";
import {IData} from "../data/IMetadata.ts";

import {Entries} from "./Entries.tsx";

export const EntryArray = ({label, value}: {label: string; value: IData[]}) => {
	const [collapsed, setCollapsed] = useState(true);
	const translator = useContext(TranslatorContext);

	let content: object;
	let rows: object[] = [];
	if (["core_contributions", "division_contributions"].indexOf(label) !== -1) {
		content = <td>
			<Card
				option={new Option({id: "", detailed: false, label: JSON.stringify(value)})}
				selected={true}
				collapsed={false}
			/>
		</td>;
	} else if (value.length !== 0 && value[0].hasOwnProperty("label")) {
		content = <td>{value[0].description}</td>;
		rows = value.slice(1).map(v => <tr>
			<td>{v.description}</td>
		</tr>);
	} else {
		content = <td className="metadata-table__sub">
			{value.map(((v: IData) => <Entries data={v}/>))}
		</td>;
	}

	return <>
		<tr>
			<th rowSpan={collapsed ? 1 : rows.length + 1}>
				<div className="metadata-table__row-collpsable">
					{translator.translateCapitalizedFirstLetter("filters." + translator.convertName(label))}:
					{!collapsed && <a
						onClick={() => setCollapsed(true)}
						className="metadata-table__switch metadata-table__switch--collapse"
					>
						{translator.translate("buttons.collapse")}
					</a>}
				</div>
			</th>
			{!collapsed && content}
			{collapsed && <td><a
				onClick={() => setCollapsed(false)}
				className="metadata-table__switch metadata-table__switch--expand"
			>
				{translator.translate("buttons.expand")}
			</a></td>}
		</tr>
		{!collapsed && rows}
	</>;
};
