<?php // no direct access
defined('_JEXEC') or die('Restricted access');

$language = JFactory::getLanguage()->getTag() === 'en-GB' ? 'en-GB' : 'cs-CZ';

JFactory::getLanguage()->load('mod_nfifilters', JPATH_SITE, $language, true);

include_once __DIR__ . "/../vite_helpers.php";

$languages = \Joomla\CMS\Language\LanguageHelper::getLanguages('lang_code');
$languageCode = $language === 'cs-CZ' ? '' : $languages[$language]->sef;

$url = JURI::root() . $languageCode;

?>
<div id='full-content'>
	<div id="shadow-vertical"></div>
	<div id="right-content">
		<div id="shadow-vertical"></div>
		<div id="top-content">
			<h1><?php echo JText::_('TITLE'); ?></h1>
			<p><?php echo JText::_('SUBTITLE'); ?></p>
		</div>
		<div
			id="mid-content"
			class="nfi-filters"
			data-lang="<?= $language ?>"
			data-base-url="<?=trim(JURI::root(), '/')?>"
			data-base-lang-url="<?=trim($url, '/')?>"
		></div>
	</div>
</div>

<?= vite('main.tsx') ?>
