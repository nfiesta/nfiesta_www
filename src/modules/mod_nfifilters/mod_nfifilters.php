<?php
/**
 * NFI Filters! Module Entry Point
 *
 * @package    NFI.web
 * @subpackage Modules
 * @link http://nil.uhul.cz
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

// Include the syndicate functions only once
require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'configuration.php' );
require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'nfiquery.php' );

$config = new NFIConfig;
$db = new NFIQuery;
$db->connect($config->db_config);

$jinput = JFactory::getApplication()->input;




require( JModuleHelper::getLayoutPath( 'mod_nfifilters' ) );
$db->close();
?>
