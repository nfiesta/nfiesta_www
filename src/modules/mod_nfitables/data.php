<?php
/**
 * NFI Tables! Module Entry Point
 *
 * @package    NFI.web
 * @subpackage Modules
 * @link http://nil.uhul.cz
 */

$jlang = JFactory::getLanguage();
$lang_nfitables = $jlang->getTag();

if($lang_nfitables == 'en-GB') {
JFactory::getLanguage()->load('mod_nfitables', JPATH_SITE, 'en-GB', true);
//$_SEARCH = JText::_( 'SEARCH' );
//die("die z data: '$_SEARCH' ");
//$_printChart = JText::_( 'PRINTCHART' );
//die("die z data: '$_printChart' ");
//$_markRegion = JText::_( 'MARKREGION' );
//die("die z data: '$_markRegion' ");
}
else {
JFactory::getLanguage()->load('mod_nfitables', JPATH_SITE, 'cs-CZ', true);
//$_SEARCH = JText::_( 'SEARCH' );
//die("die z data: '$_SEARCH' ");
//$_printChart = JText::_( 'PRINTCHART' );
//die("die z data: '$_printChart' ");
//$_markRegion = JText::_( 'MARKREGION' );
//die("die z data: '$_markRegion' ");
}

class modNfiTables {

	static public $langCode;

	/**
	 *
	 * @param unknown $estimateData
	 * @return multitype:number multitype:string multitype:string   multitype:
	 */
	public static function formatDataForTable($estimateData, $hasDenom) {
		$dataWithParams = array (
				"sEcho" => 3,
				"pageLength" => 14,
				"iTotalRecords" => 57,
				"iTotalDisplayRecords" => 57,
				"aoColumnDefs" => array (
					array(
							"aTargets" => [-1,-2,-3,-4],
							"sClass" => "alignRight"
							)

				),
				"language" => array (
						//'search' => 'Hledat&nbsp;:',
            'search' => JText::_( 'SEARCH' ),
						//'lengthMenu' => 'Zobraz záznamů _MENU_',
            'lengthMenu' => JText::_( 'LENGTHMENU' ),
						'thousands' => '.',
						'decimal' => ',',
						//'loadingRecords' => 'Nahrávám záznamy...',
            'loadingRecords' => JText::_( 'LOADINGRECORDS' ),
						//'processing' => 'Provádím...',
            'processing' => JText::_( 'PROCESSING' ),
						//'zeroRecords' => 'Žádné záznamy nebyly nalezeny',
            'zeroRecords' => JText::_( 'ZERORECORDS' ),
						//'info' => 'Zobrazuji _START_ až _END_ z celkem _TOTAL_ záznamů',
            'info' => JText::_( 'INFO' ),
						//'infoEmpty' => 'Zobrazuji 0 až 0 z 0 záznamů',
            'infoEmpty' => JText::_( 'INFOEMPTY' ),
						//'infoFiltered' => '(filtrováno z celkem _MAX_ záznamů)',
            'infoFiltered' => JText::_( 'INFOFILTERED' ),
						'infoPostFix' => '',
						'url' => '',
						'paginate' => array (
								//'first' => 'První',
                'first' => JText::_( 'FIRST' ),
								//'last' => 'Poslední',
                'last' => JText::_( 'LAST' ),
								//'next' => 'Další',
                'next' => JText::_( 'NEXT' ),
								//'previous' => 'Předchozí'
                'previous' => JText::_( 'PREVIOUS' )
						)
				),
				"aaData" => array_map(function ($data) use ($hasDenom) {
					$estimationCell = explode(' – ', $data['estimation_cell']);

					$data = [
						'<div class="value">' . $estimationCell[0] . '<small>' . $estimationCell[1] . '</small></div>',
						self::convertArrayValues($data['variable_area_domain_num']),
						self::convertArrayValues($data['variable_sub_population_num']),
						self::convertArrayValues($data['variable_area_domain_denom']),
						self::convertArrayValues($data['variable_sub_population_denom']),
						self::formatNumber($data['point_estimate']),
						self::formatNumber($data['standard_deviation']),
						self::formatNumber($data['variation_coeficient']),
						self::formatNumber($data['interval_estimation']),
					];

					if (!$hasDenom) {
						unset($data[3]);
						unset($data[4]);
					}

					return array_values($data);
				}, $estimateData),
		);

		return $dataWithParams;
	}

	private static function formatNumber(int $number = null): string {
		if ($number === null) {
			return '';
		}

		if (modNfiTables::$langCode === 'cs-CZ') {
			return number_format($number, 2, ',', ' ');
		}

		return number_format($number, 2, '.', ',');
	}

	private static function convertArrayValues($data) {
		return '<div class="value">' . implode('</div><div class="value">', explode(';', $data)) . '</div>';
	}

	public static function createTableFunction($estimateData, $hasDenom) {
		$json = json_encode ( modNfiTables::formatDataForTable($estimateData, $hasDenom) );
		$htmlTableFunction = '$("#nfitables").dataTable(' . $json . ');';

		return $htmlTableFunction;
	}

	public static function parseTextEstimateWithComma($value) {
		$number = floatval ( str_replace ( ',', '.', str_replace ( '.', '', $value ) ) );
		return $number;
	}

	public static function createMapFunction($graphTitle) {

    // dynamicke plneni promennych z CS nebo EN php souboru
    $_graphTitleNFI  = JText::_( 'GRAPHTITLENFI' );
    $_forestCover = JText::_( 'FORESTCOVER' );
    $_forestCoverNFI = JText::_( 'FORESTCOVERNFI' );
    $_markRegion = JText::_( 'MARKREGION' );
    $_treeCoverRS = JText::_( 'TREECOVERRS' );
    $_landCoverRS = JText::_( 'LANDCOVERRS' );
    $_treeCover = JText::_( 'TREECOVER' );
    $_treeCover1 = JText::_( 'TREECOVER1' );
    $_treeCover2 = JText::_( 'TREECOVER2' );
    $_treeCover3 = JText::_( 'TREECOVER3' );
    $_treeCover4 = JText::_( 'TREECOVER4' );
    $_treeCover5 = JText::_( 'TREECOVER5' );
    $_landCover = JText::_( 'LANDCOVER' );
    $_landCover1 = JText::_( 'LANDCOVER1' );
    $_landCover2 = JText::_( 'LANDCOVER2' );
    $_landCover3 = JText::_( 'LANDCOVER3' );
    $_landCover4 = JText::_( 'LANDCOVER4' );
    $_landCover5 = JText::_( 'LANDCOVER5' );
    //-----------------------------------------------------

		$htmlMapFunction = <<<EOHTML
var map = L.map('map', {
		    zoomControl: false
		});
		
		var graphMarkers = L.layerGroup([]);
		var legendSquare;
		var legendCircle;

		var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
		var okrajLesaUrl = 'http://eagri.cz/public/app/uhul/TMS{s}/NIL-OL-EPSG3857/{z}/{x}/{y}';
		var krajinnyPokryvUrl = 'http://eagri.cz/public/app/uhul/TMS{s}/NIL-PL-EPSG3857/{z}/{x}/{y}';
		//TODO Zmena vrstvy
// 		var holinyUrl = 'http://eagri.cz/public/app/uhul/TMS{s}/NIL-H-EPSG3857/{z}/{x}/{y}';
		var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
		var nilAttrib = 'Map data © <a href="http://nil.uhul.cz">nil.uhul.cz</a>';
		var osm = new L.TileLayer(osmUrl, {
		  minZoom: 5,
		  maxZoom: 12,
		  attribution: osmAttrib
		});

		var okrajLesa = new L.TileLayer(okrajLesaUrl, {
		  minZoom: 7,
		  maxZoom: 18,
		  attribution: nilAttrib,
		  subdomains: '12'
		});

		//TODO Zmena vrstvy
// 		var holiny = new L.TileLayer(holinyUrl, {
// 		  minZoom: 6,
// 		  maxZoom: 18,
// 		  attribution: nilAttrib,
// 		  subdomains: '12'
// 		});

		var krajinnyPokryv = new L.TileLayer(krajinnyPokryvUrl, {
		  minZoom: 6,
		  maxZoom: 18,
		  attribution: nilAttrib,
		  subdomains: '12'
		});

		var lat = 50;
		var lng = 15;
		var zoom = 7;

		// start the map in Middle Europe
		map.setView(new L.LatLng(lat, lng), zoom);

		// custom zoom bar control that includes a Zoom Home function
		L.Control.zoomHome = L.Control.extend({
		  options: {
		    position: 'topleft',
		    zoomInText: '+',
		    zoomInTitle: 'Zoom in',
		    zoomOutText: '-',
		    zoomOutTitle: 'Zoom out',
		    zoomHomeText: '<i class="fa fa-home" style="line-height:1.65;"></i>',
		    zoomHomeTitle: 'Zoom home'
		  },
		
		  onAdd: function(map) {
		    var controlName = 'gin-control-zoom',
		      container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
		      options = this.options;

		    this._zoomInButton = this._createButton(options.zoomInText, options.zoomInTitle,
		      controlName + '-in', container, this._zoomIn);
		    this._zoomHomeButton = this._createButton(options.zoomHomeText, options.zoomHomeTitle,
		      controlName + '-home', container, this._zoomHome);
		    this._zoomOutButton = this._createButton(options.zoomOutText, options.zoomOutTitle,
		      controlName + '-out', container, this._zoomOut);

		    this._updateDisabled();
		    map.on('zoomend zoomlevelschange', this._updateDisabled, this);

		    return container;
		  },

		  onRemove: function(map) {
		    map.off('zoomend zoomlevelschange', this._updateDisabled, this);
		  },

		  _zoomIn: function(e) {
		    this._map.zoomIn(e.shiftKey ? 3 : 1);
		  },

		  _zoomOut: function(e) {
		    this._map.zoomOut(e.shiftKey ? 3 : 1);
		  },

		  _zoomHome: function(e) {
		    map.setView([lat, lng], zoom);
		  },

		  _createButton: function(html, title, className, container, fn) {
		    var link = L.DomUtil.create('a', className, container);
		    link.innerHTML = html;
		    link.href = '#';
		    link.title = title;

		    L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
		      .on(link, 'click', L.DomEvent.stop)
		      .on(link, 'click', fn, this)
		      .on(link, 'click', this._refocusOnMap, this);

		    return link;
		  },

		  _updateDisabled: function() {
		    var map = this._map,
		      className = 'leaflet-disabled';

		    L.DomUtil.removeClass(this._zoomInButton, className);
		    L.DomUtil.removeClass(this._zoomOutButton, className);

		    if (map._zoom === map.getMinZoom()) {
		      L.DomUtil.addClass(this._zoomOutButton, className);
		    }
		    if (map._zoom === map.getMaxZoom()) {
		      L.DomUtil.addClass(this._zoomInButton, className);
		    }
		  }
		});
		// add the new control to the map
		var zoomHome = new L.Control.zoomHome();
		zoomHome.addTo(map);

		var nuts1Layer = L.geoJson(nuts1, {
			style : style,
			onEachFeature : onEachFeature
		});
				
		var nuts3Layer = L.geoJson(nuts3,{style: style, onEachFeature: onEachFeature});
				
		var nutsGroup = new L.LayerGroup();
		nutsGroup.addLayer(nuts3Layer);

		nutsGroup.addTo(map);
		//Info div
		var info = L.control();

		info.onAdd = function(map) {
			this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
			this.update();
			return this._div;
		};
    
		//Info div - method that we will use to update the control based on feature properties passed
		info.update = function(props) {
			this._div.innerHTML = '<h4>$_forestCover</h4>'
					+ (props ? '<b>' + props.description
							+ '</b><br /> Lesnatost ' + props.lesnatost_bo
							+ ' %' : '$_markRegion');
		};

		//Legend
		var legend = L.control({
			position : 'bottomleft'
		});

		legend.onAdd = function(map) {

			var div = L.DomUtil.create('div', 'info legend'), grades = [ 31.0,
					33.0, 36.0, 39.0, 41.0 ], labels = [];

			div.innerHTML += '<h3>$_forestCover (%)</h3>'

			// loop through our density intervals and generate a label with a colored square for each interval
			for (var i = 0; i < grades.length; i++) {
				div.innerHTML += '<i style="background:'
						+ getColor(grades[i] + 1)
						+ '"></i> '
						+ grades[i]
						+ (grades[i + 1] ? '&ndash;' + grades[i + 1]
								+ '</span><br>' : '+');
			}

			return div;
		};

		legend.addTo(map);

		info.addTo(map);
		
		var layerSelect = document.getElementById("layerSelect");

		var overlayMaps = {
		  "$graphTitle ($_graphTitleNFI)" : graphMarkers,
		  "$_forestCoverNFI" : nutsGroup,
		  "$_treeCoverRS": okrajLesa,
		  "$_landCoverRS": krajinnyPokryv
		};

		L.control.layers(null, overlayMaps,{position: 'topright'}).addTo(map);

		map.addLayer(osm);

		//Legend
		var legendKrajinnyPokryv = L.control({
		  position : 'bottomright'
		});

		legendKrajinnyPokryv.onAdd = function(map) {

		  var div = L.DomUtil.create('div', 'info legend-tile');


				div.innerHTML += '<h3>$_landCover</h3>'

		    div.innerHTML += '<span><i style="background:' + '#a97606' + '"></i> $_landCover1</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#ffbf00' + '"></i> $_landCover2</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#009531' + '"></i> $_landCover3</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#a0db46' + '"></i> $_landCover4</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#2c5bc8' + '"></i> $_landCover5</span><br><br>';

		  return div;
		};

		//Legend
		var legendDrevinnyPokryv = L.control({
		  position : 'bottomright'
		});

		legendDrevinnyPokryv.onAdd = function(map) {

		  var div = L.DomUtil.create('div', 'info legend-tile');


				div.innerHTML += '<h3>$_treeCover</h3>'

		    div.innerHTML += '<span><i style="background:' + '#33a02c' + '"></i> $_treeCover1</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#bba054' + '"></i> $_treeCover2</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#939392' + '"></i> $_treeCover3</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#b2df8a' + '"></i> $_treeCover4</span><br><br>';

		    div.innerHTML += '<span><i style="background:' + '#df6e1d' + '"></i> $_treeCover5</span><br><br>';

		  return div;
		};

		//Legend
//TODO Zmena vrstvy
// 		var legendDetekceTezeb = L.control({
// 		  position : 'bottomright'
// 		});

// 		legendDetekceTezeb.onAdd = function(map) {

// 		  var div = L.DomUtil.create('div', 'info legend-tile');


// 				div.innerHTML += '<h3>Detekce těžeb</h3>'

// 		    div.innerHTML += '<span><i style="background:' + '#fdf97e' + '"></i> Nová holina</span><br><br>';

// 		    div.innerHTML += '<span><i style="background:' + '#7f7fff' + '"></i> Ředina</span><br><br>';

// 		  return div;
// 		};

		legendDrevinnyPokryv.addTo(map);
		legendKrajinnyPokryv.addTo(map);
		//TODO Zmena vrstvy
// 		legendDetekceTezeb.addTo(map);

		// Hide all the elements in the DOM that have a class of "legend-tile"
		$('.legend-tile').hide();

		var toggle = L.easyButton({position: 'topright',
		  states: [{
		    stateName: 'add-markers',
		    icon: '<strong>L</strong>',
		    title: 'add random markers',
		    onClick: function(control) {
		      $(".legend-tile").show();
		      control.state('remove-markers');
		    }
		  }, {
		    icon: '<strong>X</strong>',
		    stateName: 'remove-markers',
		    onClick: function(control) {
		      $(".legend-tile").hide();
		      control.state('add-markers');
		    },
		    title: 'remove markers'
		  }]
		});
		toggle.addTo(map);
EOHTML;

		return $htmlMapFunction;
	}

	public static function createGraphFunction($estimateData, $ajaxparam, $metadata, $attributes) {

		$firstAttributeLabel = $attributes[0]['label'];
		$firstAttributeColor = $attributes[0]['color'];
		$description = ucfirst($metadata['result']['label']);
		$unitOfMeasure = $metadata['unit_of_measure']['label'];

        $_gdomain_level = $metadata['gdomain_level']['label'];

        if ($_gdomain_level == 'NUTS 0 + 1 – stát' || $_gdomain_level == 'NUTS 0 + 1 – state')
        {
            $_Rotation = 0;
            $_marginBottom = 100;
        }
        else if ($_gdomain_level == 'NUTS 2 – region' || $_gdomain_level == 'NUTS 2 – area')
        {
             $_Rotation = -45;
             $_marginBottom = 150;
        }
        else if ($_gdomain_level == 'NUTS 3 – kraj' || $_gdomain_level == 'NUTS 3 – region')
        {
             $_Rotation = -45;
             $_marginBottom = 180;
        }
        else if ($_gdomain_level == 'LAU 1 – okres' || $_gdomain_level == 'LAU 1 – district')
        {
             $_Rotation = -45;
             $_marginBottom = 180;
        }
        else if ($_gdomain_level == 'PLO – přírodní lesní oblasti' || $_gdomain_level == 'NFA – Natural Forest Areas')
        {
             $_Rotation = -45;
             $_marginBottom = 250;
        }
        else if ($_gdomain_level == 'OPLO – obvody přírodních lesních oblastí' || $_gdomain_level == 'DNFA – districts of Natural Forest Areas')
        {
             $_Rotation = -45;
             $_marginBottom = 200;
        }
        else
        {
            $_Rotation = -45;
            $_marginBottom = 180;
        }

		$ajaxparams = '';

		if (is_int($ajaxparam)) {
			$ajaxparams = 'pc: ' . $ajaxparam;
		} else if (is_array($ajaxparam)) {
			foreach ($ajaxparam as $key => $value) {
				$ajaxparams .= $key . ': ' . $value . ',';
			}
			$ajaxparams = rtrim($ajaxparams, ",");
		}

		$data = array_values ( pg_fetch_all ( $estimateData ) );

		$absoluteParam = ",graph:true";

		$categories = array ();
		$values = array ();
		$interval = array ();

		foreach ( $data as $row ) {

			if(isset($row[6]) && $row[6] != $firstAttributeLabel) {
				continue;
			}

			array_push ( $categories, $row [0] );
			array_push ( $values, modNfiTables::parseTextEstimateWithComma ( $row [1] ) );
			$min_max = array ();
			array_push ( $min_max, modNfiTables::parseTextEstimateWithComma ( $row [3] ), modNfiTables::parseTextEstimateWithComma ( $row [4] ) );
			array_push ( $interval, $min_max );
		}
		$categoriesJSON = json_encode ( $categories );
		$valuesJSON = json_encode ( $values );
		$intervalJSON = json_encode ( $interval );

    // dynamicke plneni promennych z CS nebo EN php souboru
    $_printChart = JText::_( 'PRINTCHART' );
    $_downloadPNG = JText::_( 'DOWNLOADPNG' );
    $_downladJPEG = JText::_( 'DOWNLOADJPEG' );
    $_downloadPDF = JText::_( 'DOWNLOADPDF' );
    $_downloadSVG = JText::_( 'DOWNLOADSVG' );
    $_downloadCSV = JText::_( 'DOWNLOADCSV' );
    $_exportButtonTitle = JText::_( 'EXPORTBUTTONTITLE' );
    $_contextButtonTitle = JText::_( 'CONTEXTBUTTONTITLE' );
    $_loading = JText::_( 'LOADING' );
    $_printButtonTitle = JText::_( 'PRINTBUTTONTITLE' );
    $_resetZoom = JText::_( 'RESETZOOM' );
    $_resetZoomTitle = JText::_( 'RESETZOOMTITLE' );
    $_pointEstimate = JText::_( 'POINTESTIMATE' );
    $_limitesName = JText::_( 'LIMITESNAME' );
    $_limitesPointFormat = JText::_( 'LIMITESPOINTFORMAT' );
    $_wordTO = JText::_( 'WORDTO' );
    $_exportGraph = JText::_( 'EXPORTGRAPH' );
    //-----------------------------------------------------

		$htmlGraphFunction = <<<EOHTML
        $(document).ready(function() {
    			    Highcharts.setOptions({
    					credits: {
    						href: 'http://nil.uhul.cz',
    						text: 'nil.uhul.cz'
  						},
				        lang: {
				            months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen',
				                          'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
				            weekdays: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 
				                             'Čtvrtek', 'Pátek', 'Sobota'],
				            shortMonths: ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čvn', 'Čvc',
				                                'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
				            decimalPoint: ',',
    						//printChart: 'Vytisknout grafik',
				            //downloadPNG: 'Stáhnout jako obrázek PNG',
				            //downloadJPEG: 'Stáhnout jako obrázek JPEG',
				            //downloadPDF: 'Stáhnout jako PDF',
				            //downloadSVG: 'Stáhnout jako obrázek SVG',
				            //exportButtonTitle: 'Exportovat graf',
				            //contextButtonTitle: 'Exportovat graf',
				            //loading: 'Nahrávám...',
				            //printButtonTitle: 'Vytisknout graf',
				            //resetZoom: 'Původní zoom',
				            //resetZoomTitle: 'Zoom na 1:1',
                  printChart: '$_printChart',                    
				            downloadPNG: '$_downloadPNG',
				            downloadJPEG: '$_downladJPEG',
				            downloadPDF: '$_downloadPDF',
				            downloadSVG: '$_downloadSVG',
				            exportButtonTitle: '$_exportButtonTitle',
				            contextButtonTitle: '$_contextButtonTitle',
				            loading: '$_loading',
				            printButtonTitle: '$_printButtonTitle',
				            resetZoom: '$_resetZoom',
				            resetZoomTitle: '$_resetZoomTitle',                    
				            thousandsSep: '',
				            
				        }
    					});

    			var options = {
                    chart: {
                        renderTo: 'nfigraph',
                        marginRight: 25,
                        marginLeft: 100,
                        marginBottom: '$_marginBottom',
                        spacingLeft: 0,
                        spacingBottom: 20

                },

                title: {
                    useHTML: true,

                    text: '$description'
    			},
    			xAxis: [{
    				categories: $categoriesJSON,
    				labels:{
    					rotation: $_Rotation,
                		formatter: function(){
                    		if (this.value.length > 100){
								return this.value.substr(this.value.indexOf('-') + 1, this.value.length).trim();
                    		} else {
                         		return this.value;   
                    		}    
                    	}                    
                }
    			}],
    			yAxis: [{ // Primary yAxis
    				labels: {
    					format: '{value}',
    					useHTML: true,
    					formatter: function(){
                    		if (this.value > 1000000){
								return Math.round(this.value/1000000) + '\*10<sup>6</sup>';
                    			} else {
                         		return this.value;   
                    			}
                    	},
    					style: {
    						color: Highcharts.getOptions().colors[1]
    					}
    				},
    				title: {
					useHTML: true,
					text: '$_pointEstimate [$unitOfMeasure]',
					rotation: 270,
					margin: 18,
    					style: {
    						color: Highcharts.getOptions().colors[1],
    					}
					
    				}
    			}],
    	
    			tooltip: {
    				shared: true,
				useHTML: true
    			},
    	
    			series: [{
    				name: '$firstAttributeLabel',
    				type: 'column',
    				yAxis: 0,
				color: '$firstAttributeColor',
    				data: $valuesJSON,
    				tooltip: {
					pointFormat: '<span style="font-weight: bold; color: {series.color}">{series.name}</span>: <b>{point.y:.2f} $unitOfMeasure</b> ',
    					useHTML: true
    				}
    			}, {
    				name: '$_limitesName',
    				type: 'errorbar',
    				yAxis: 0,
    				data: $intervalJSON,
    				tooltip: {
    					valueDecimals: 2,
    					pointFormat: '($_limitesPointFormat {point.low} $_wordTO {point.high} $unitOfMeasure)<br/>',
    					useHTML: true
    				}
    			}],
    			
    			   			
    			exporting: {
    				enabled: false, 
					allowHTML: true,
					filename: 'NFI_graph',
					width: 1100,
					buttons: {
		                contextButton: {
		                	text: '$_exportGraph',
		                	symbol: 'circle',
		                    menuItems: [{
		                        textKey: 'printChart',
		                        onclick: function () {
		                            this.print();
		                        }
		                    }, {
		                        separator: true
		                    }, {
		                        textKey: 'downloadPNG',
		                        onclick: function () {
		                            this.exportChart();
		                        }
		                    }, {
		                        textKey: 'downloadJPEG',
		                        onclick: function () {
		                            this.exportChart({
		                                type: 'image/jpeg'
		                            });
		                        }
		                    //}, {
		                     //   textKey: 'downloadPDF',
		                     //   onclick: function () {
		                     //       this.exportChart({
		                     //           type: 'application/pdf'
		                     //       });
		                     //   }
		                    }, {
		                        textKey: 'downloadSVG',
		                        onclick: function () {
		                            this.exportChart({
		                                type: 'image/svg+xml'
		                            });
		                        }
		                    }
                            , {
		                        text: Highcharts.getOptions().lang.downloadCSV || "$_downloadCSV",
		                        onclick: function () {
		                            Highcharts.post('http://www.highcharts.com/studies/csv-export/csv.php', {
		                                csv: this.getCSV()
		                           });
		                        }
		                    }
                            ]
		                }
		            }
				}
        
                };
    					
                chart = new Highcharts.Chart(options);

			changeGraph = function(attributeParams){
				var params = '{ '+ '$ajaxparams';
				params = params + '$absoluteParam';
				params = params + ',' + attributeParams;
				if (params.endsWith(',')) {
					params = params.slice(0, -1);
					}
				params = params + ' }';

				//Oprava JSON struktury z {propery:100} na {"property":100} , aby byl JSON validni
				params = params.replace(/(['"])?([a-zA-Z0-9_]+)(['"])?:/g, '"$2": ');
				params = $.parseJSON(params);
     				var chartCategories = [];
     				var chartValues = [];
     				var chartIntervals = [];
    				var chart = new Highcharts.Chart(options); 
					$.ajax({
						url: "index.php?option=com_nfidata&format=json",
						context: document.body, 
					data: params,
        				dataType: 'json'
						}).done(function(data) {
			                $.each(data["data"], function(i, item){
			                	chartCategories.push("\"" + item[0] + "\"");
						//null prevent
							if (item[1] === null) {
								item[1] = String(item[1]);
							}
							if (item[3] === null) {
								item[3] = String(item[3]);
							}
							if (item[4] === null) {
								item[4] = String(item[4]);
							}
        						chartValues.push(item[1]);
							chartIntervals.push("[" + item[3] + "," +item[4] + "]")
				            });
				            var jsonCategories = eval('[' + chartCategories.toString() + ']');
        					var jsonData = eval('[' + chartValues.toString() + ']');
        					var jsonIntervalData = eval('[' + chartIntervals.toString() + ']');

							attributeDescription = getAttributesDescription();

        					options.xAxis = [{
        					    categories: jsonCategories,
			    			    labels:{
    								rotation: $_Rotation,
                					formatter: function(){
                    							if (this.value.length > 100) {
												return this.value.substr(this.value.indexOf('-') + 1, this.value.length).trim();
                    							} else {
                         						return this.value;   
                    							}    
                    				}                    
						 }
						}];
						
							//Mezera za strednik
							attributeDescription = attributeDescription.replace(/;/g, "; ");
						
							options.series = [{
							name: attributeDescription,
							yAxis: 0,
							type: 'column',
							color: data["color"],
        						data: jsonData,
        						tooltip: {
								pointFormat: '<span style="font-weight: bold; color: {series.color}">{series.name}</span>: <b>{point.y:.2f} $unitOfMeasure</b> ',
        							useHTML: true
    							}
    						}, {
    							name: '$_limitesName',
    							type: 'errorbar',
    							yAxis: 0,
    							data: jsonIntervalData,
    							tooltip: {
    								valueDecimals: 2,
    								pointFormat: '($_limitesPointFormat {point.low} $_wordTO {point.high} $unitOfMeasure)<br/>',
    								useHTML: true
    							}
							}];
							var chart = new Highcharts.Chart(options);
						});
				};
        });
EOHTML;

		return $htmlGraphFunction;
	}

	function startsWith($haystack, $needle)
	{
		return $needle === "" || strpos($haystack, $needle) === 0;
	}
}

modNfiTables::$langCode = $lang_nfitables;
