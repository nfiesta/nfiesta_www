<?php
/**
 * NFI Tables! Module Entry Point
 *
 * @package    NFI.web
 * @subpackage Modules
 * @link http://nil.uhul.cz
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

// Include the syndicate functions only once
require_once( dirname(__FILE__). DS .'data.php' );
require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'configuration.php' );
require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'nfiquery.php' );

$config = new NFIConfig;
$db = new NFIQuery;
$db->connect($config->db_config);
// Getting language Tag from Joomla
$lang = JFactory::getLanguage()->getTag();
$jinput = JFactory::getApplication()->input;

// GET parameters
$group = $jinput->get('group', '', 'string');

if (!$group) {
	JFactory::getApplication()->redirect("");
}

$estimateData = $db->getUserQuery($lang, $_GET['group']);
$metadata = $db->getUserMetadata($lang, $_GET['group']);
$translations = $db->getTranslations($lang);
$translations = array_combine(array_column($translations, 'gui_header'), array_column($translations, 'label'));
$hasDenom = $estimateData[0]
	&& $estimateData[0]['variable_area_domain_denom'] !== null
	&& $estimateData[0]['variable_sub_population_denom'] !== null;
$tableFunction = modNfiTables::createTableFunction ( $estimateData, $hasDenom );

if (isset($_GET['export'])) {
	$csvHandle = fopen('php://memory', 'w');
	fputcsv($csvHandle, array_keys($estimateData[0]), ';');
	foreach ($estimateData as $data) {
		fputcsv($csvHandle, $data, ';');
	}

	fseek($csvHandle, 0);
	$csvContent = stream_get_contents($csvHandle);
	fclose($csvHandle);

	$zipTempFile = tempnam("/tmp", "nil_export");
	$timestamp = date('Y-m-d H-i-s');

	$zip = new ZipArchive();
	$zip->open($zipTempFile,  ZipArchive::CREATE);
	$zip->addFromString('nfi_results_metadata [' . $timestamp . '].json', json_encode($metadata));
	$zip->addFromString('nfi_results [' . $timestamp . '].csv', "\xEF\xBB\xBF" . $csvContent);
	$zip->close();

	header('Content-type: text/csv; charset=UTF-8');
	header('Content-Disposition: attachment; filename="nfi_results [' . $timestamp . '].zip"');
	readfile($zipTempFile);

	@unlink($zipTempFile);
	exit;
}

require( JModuleHelper::getLayoutPath( 'mod_nfitables' ) );
$db->close();
