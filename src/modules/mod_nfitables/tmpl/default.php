<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//JFactory::getLanguage()->load('mod_nfitables', JPATH_SITE, 'cs-CZ', true);


// pridani nove begin
if (!defined('DS')) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

require_once( JPATH_LIBRARIES . DS .'nfidatabase' . DS . 'configuration.php' );
include_once __DIR__ . "/../../mod_nfifilters/vite_helpers.php";

$config = new NFIConfig;
$dbconfig = $config->db_config;
$db_server = $dbconfig["server"];
$db_database = $dbconfig["database"];
$db_username = $dbconfig["username"];
$db_password = $dbconfig["password"];
// pridani nove end


$jlang = JFactory::getLanguage();
$lang_nfitables = $jlang->getTag();

if($lang_nfitables == 'en-GB') {
	JFactory::getLanguage()->load('mod_nfitables', JPATH_SITE, 'en-GB', true);
}
else {
	JFactory::getLanguage()->load('mod_nfitables', JPATH_SITE, 'cs-CZ', true);
}

use Joomla\CMS\Uri\Uri;
$currentUrl = Uri::getInstance()->toString();

?>

<div id='full-content'>
	<div id='shadow-vertical'></div>
	<div id='right-content'>
		<div id='top-content'>
			<h1><?php echo ucfirst(rtrim(($metadata->topic->label),".")); ?></h1>
			<p><?php echo ucfirst($metadata->estimate_type->description); ?> [<?php echo $metadata->unit_of_measurement->label; ?>].</p>
		</div>
	</div>
	<div id='left-content' class="nfiFilters">
		<table id="nfitables" class="result-table" >
			<thead>
			<tr>
				<th style="min-width: 120px;">
					<div class="result-table__header">
						<span><?= $translations['region']; ?></span>
					</div>
				</th>
				<th>
					<div class="result-table__header">
						<span>
							<?= $translations['area_domain_category']; ?>
							(<?= $translations['numerator']; ?>)
						</span>
					</div>
				</th>
				<th>
					<div class="result-table__header">
						<span>
							<?= $translations['sub_population_category']; ?>
							(<?= $translations['numerator']; ?>)
						</span>
					</div>
				</th>
				<?php if ($hasDenom): ?>
				<th>
					<div class="result-table__header">
						<span>
							<?= $translations['sub_population_category']; ?>
							(<?= $translations['denominator']; ?>)
						</span>
					</div>
				</th>
				<th>
					<div class="result-table__header">
						<span>
							<?= $translations['area_domain_category']; ?>
							(<?= $translations['denominator']; ?>)
						</span>
					</div>
				</th>
				<?php endif; ?>
				<th>
					<div class="result-table__header">
						<span><?= $translations['point_estimate']; ?></span>
					</div>
				</th>
				<th>
					<div class="result-table__header">
						<span><?= $translations['standard_deviation']; ?></span>
					</div>
				</th>
				<th>
					<div class="result-table__header">
						<span><?= $translations['variation_coeficient']; ?></span>
					</div>
				</th>
				<th>
					<div class="result-table__header">
						<span><?= $translations['interval_estimation']; ?></span>
					</div>
				</th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<a href="<?=$currentUrl?>&export" class="filter__button">
			Exportovat data (.zip)
		</a>

		<br><br>

		<!-- Skripty -->
		<script type="text/javascript" charset="utf8" src="templates/nil/script/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" charset="utf8" src="templates/nil/script/jquery.dataTables.min.js"></script>
		<script>
			$(function(){
				var dt = <?=$tableFunction?>
				console.log($("#nfitables").DataTable().columns.adjust());
			})
		</script>

		<!-- Informace k tabulce -->
		<h3><?php echo JText::_( 'TABLE_INFORMATION' ); ?></h3>
		<div class="metadata nfiFilters" data-lang="<?= $lang_nfitables ?>" data-base-url="<?=trim(JURI::root(), '/')?>"></div>
	</div>
</div>

<?= vite('main.tsx') ?>
