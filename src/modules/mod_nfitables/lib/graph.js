function createGraphCategoryOptions(params,categoryParams) {
	var categoryIdx = Object.keys(categoryParams).length;

	$.extend(params, categoryParams);

    $.ajax({
        url: "index.php?option=com_nfidata&format=json",
        context: document.body,
        data: params,
        dataType: 'json'
    }).done(function(data) {
    //1 odpovida categorii bez rozliseni
	if (data.length > 1 || data[0].res_id != 1) {

		$('#categorySelect' + categoryIdx).remove();

		$('#graphCategories').append('<select aria-controls="nfitables" id="categorySelect' + categoryIdx + '" onchange="graphOptionChanged(this);"></select>');

		$.each(data, function(idx, obj) {
			$('#categorySelect' + categoryIdx).append('<option value="' + obj.res_id + '">' + obj.res_label + '</option>')
		});

		var category = $.parseJSON('{ "attribute' + categoryIdx + '": ' + data[0].res_id + ' }');
		$.extend(categoryParams, category);
		createGraphCategoryOptions(params,categoryParams);
	} else {
		deleteNextSelects(categoryIdx);
		graphOptionChanged();
	}
    });
}

//maxId pokud nechceme vsechny attributy jen aktualni
//maxId je bud vyplnene nebo null
getAttributes = function(maxId) {

	var categoryParams = '';
	var id = 1;

	while (id != 0) {
		if ((id <= maxId || maxId == null) && $('#categorySelect'.concat(id)).length) {
			var optionVal = $( "#categorySelect".concat(id) + " option:selected" ).val();

			categoryParams = categoryParams + 'attribute' + id + ': ' + optionVal + ',';
			id++;
		} else {
			id = 0;
		}
	}

	return categoryParams;
};

getAttributesDescription = function() {

	var categoryParams = '';
	var id = 1;

	while (id != 0) {
		if ($('#categorySelect'.concat(id)).length) {
			var optionVal = $( "#categorySelect".concat(id) + " option:selected" ).text();
			categoryParams = categoryParams + optionVal + ';';
			id++;
		} else {
			id = 0;
		}

	}
	categoryParams = categoryParams.slice(0, -1);
	return categoryParams;
};

function deleteNextSelects(id) {
	while (id != 0) {
		if ($('#categorySelect'.concat(id)).length) {
			$('#categorySelect' + id).remove();
			id++;
		} else {
			id = 0;
		}
	}
}

function deleteAttributes(graphAjaxParams,id) {

	for (param in graphAjaxParams) {
		if (param.match(/attribute[0-9]/g)){
			delete graphAjaxParams[param];
		}
	}


}