function createCartodiagramFromPQ(pq) {
	graphMarkers.clearLayers();
	if (legendSquare != undefined) {
		map.removeControl(legendSquare);
	}
	if (legendCircle != undefined) {
		map.removeControl(legendCircle);
	}
	$
			.ajax({
				url : "/web/index.php?option=com_nfidata&format=json&pc=" + pq
						+ "&map=true",
				dataType : 'json',
				success : function(data) {
					var attributeData = data;
					var dataTitle = attributeData.data.title;
					var dataCategories = attributeData.data.categories;
					var legendScales = attributeData.legend.scales;
					var legendUnits = attributeData.legend.units;
					var legendAttribute = attributeData.legend.attribute;
					var graphLayer = L
							.geoJson(
									nuts3_graph,
									{
										style : style,
										onEachFeature : function(feature, layer) {
											for (k in dataCategories) {
												var chartSize;
												if (dataCategories[k].gdomain == feature.properties.f1) {
													for (scale in legendScales) {
														if (Number(dataCategories[k].sumOfPointEstimate.split('.')[0]) >= Number(legendScales[scale].lower) && Number(dataCategories[k].sumOfPointEstimate.split('.')[0]) <= Number(legendScales[scale].upper)) {
															chartSize = Number(legendScales[scale].size);
														}
													};
													var marker = createPieChart(
															layer._latlng,
															dataCategories[k],
															chartSize,
															legendAttribute,legendUnits);
													marker.addTo(graphMarkers);
												}
											}
										}
									});

					legendSquare = createSquareLegend(attributeData);
					legendSquare.addTo(map);

					legendCircle = createCircleLegend(attributeData);
					legendCircle.addTo(map);

					graphMarkers.addTo(map);

				}
			});
}

function createSquareLegend(attributeData) {

	var legendSquare = L.control({
		position : 'bottomleft'
	});

	legendSquare.onAdd = function(map) {

		var legendCategories = attributeData.legend.attribute;
		
		var div = L.DomUtil.create('div', 'legend info'), colors = colors, labels = labels;

		//Serazeni podle attributeid
		var byLabel = legendCategories.slice(0);
		byLabel.sort(function(a,b) {
			aa = a.attributeid.split(/(\d+)/);
		    bb = b.attributeid.split(/(\d+)/);

		    for(var x = 0; x < Math.max(aa.length, bb.length); x++) {
		      if(aa[x] != bb[x]) {
		        var cmp1 = (isNaN(parseInt(aa[x],10)))? aa[x] : parseInt(aa[x],10);
		        var cmp2 = (isNaN(parseInt(bb[x],10)))? bb[x] : parseInt(bb[x],10);
		        if(cmp1 == undefined || cmp2 == undefined)
		          return aa.length - bb.length;
		        else
		          return (cmp1 < cmp2) ? -1 : 1;
		      }
		    }
		    return 0;
		});

		for (category in byLabel) {
			if(byLabel[category].label != undefined) {
				div.innerHTML += '<span style="font-size:12;"><div style="border:1px solid;border-radius:5px;float:left;height:15px;width:15px;background:'
					+ byLabel[category].color + '"></div> &nbsp;' + byLabel[category].label + '</span><br>';
			}
		}

		return div;
	};

	return legendSquare;
}

function createCircleLegend(attributeData) {
	var legendCircle = L.control({
		position : 'bottomleft'
	});

	var labelUnique = [];
	var circleUnique = [];

	var dataCategories = attributeData.data.categories;

	var scalesLegendOptions = attributeData.legend.scales;

	var widestLegendLabel = "";
	var widestChartSize = 0;

	for (scale in scalesLegendOptions) {
		var lower = scalesLegendOptions[scale].lower;
		var upper = scalesLegendOptions[scale].upper;
		var size = scalesLegendOptions[scale].size;
		var legendText = lower + " - " + upper;

		if (legendText.length > widestLegendLabel.length) {
			widestLegendLabel = legendText;
		}

		if (size > widestChartSize) {
			widestChartSize = size;
		}

		if (typeof legendText === "undefined" || typeof size === "undefined") {
			continue;
		}

		//Push if not present
		if (labelUnique.indexOf(legendText) == -1) {
			labelUnique.push(legendText)
		}
		//Push if not present
		if (circleUnique.indexOf(size) == -1) {
			circleUnique.push(size)
		}
	}

	labelUnique.reverse();
	circleUnique.reverse();

	/*
	 * Sirka obdelniku pod legendou se odviji od delky textu v titulku nebo
	 * podle labelu o konkretni hodnoty, pokud je delsi
	 */
	var legendUnits = attributeData.legend.units;
//Nefunguje baseline-shift ve firefoxu a IE, musi se pouzit MathML
//	legendUnits = legendUnits.replace("<sup>",
//			"<tspan style=\"baseline-shift:super\">");
//	legendUnits = legendUnits.replace("<\/sup>", "<\/tspan>");
	legendUnits = legendUnits.replace("<sup>",
	"");
legendUnits = legendUnits.replace("<\/sup>", "");
	var legendTitle = attributeData.legend.title + ' (' + legendUnits + ')';
	var legendTitleFontSize = 17;
	var fontSize = 14;
	var margin = 60;

	$(		'<div id=\"titleLegendWorkingHidden\" style=\"font-size:'
					+ legendTitleFontSize
					+ ';font-weight:bold;font-family:sans-serif;position:absolute;visibility:hidden\">'
					+ legendTitle + '</div>').appendTo("div#map");
	$(		'<div id=\"labelLegendWorkingHidden\" style=\"font-size:'
					+ fontSize
					+ ';font-weight:bold;font-family:sans-serif;position:absolute;visibility:hidden\">'
					+ widestLegendLabel + '</div>').appendTo("div#map");

	var titleWidth = document.getElementById("titleLegendWorkingHidden").clientWidth
			+ margin;
	//FIXME - constant 50
	var labelWidth = document.getElementById("labelLegendWorkingHidden").clientWidth
			+ widestChartSize + 50;

	var legendBoxWidth = titleWidth > labelWidth ? titleWidth : labelWidth;

	var legendBoxHeight = circleUnique[0] + legendTitleFontSize + margin;

	legendCircle.onAdd = function(map) {

		var div = L.DomUtil.create('div', 'info legend'), circles = circleUnique, labels = labelUnique;

		var svgHTML = '<svg height="' + legendBoxHeight + '" width="'
		+ legendBoxWidth + '"><g>';
		svgHTML += '<text x="10" y="15" font-size="'
				+ legendTitleFontSize + 'px" style="font-family:signikasemibold;fill:#515151">'
				+ legendTitle + '</text>';

		var a = 0;
		var x = 30;
		var y = 2 * circleUnique[0] + legendTitleFontSize + (fontSize / 2);
		for (var i = 0; i < circles.length; i++) {
			var xpos = x;
			var ypos = y - circles[i];

			svgHTML += '<circle onmouseover="evt.target.setAttribute(\'stroke-width\', \'2\');" onmouseout="evt.target.setAttribute(\'stroke-width\', \'1\');" cx="'
					+ xpos
					+ '" cy="'
					+ ypos
					+ '" r="'
					+ circles[i]
					+ '" stroke="black" stroke-width="1" fill="#D8D8D8" >';
			svgHTML += '<title>' + labels[i] + '</title>';
			svgHTML += '</circle>';
			var lineLength = xpos + x;
			var textXPos = xpos + x + 5;
			a += 1;
			var textYPos = a * fontSize + legendTitleFontSize + 2;
			svgHTML += '<text x="' + textXPos + '" y="' + textYPos
					+ '" style="font-family:signikasemibold;fill:#515151" font-size="' + fontSize
					+ 'px">' + labels[i] + '</text>';
		}
		/*
		 * Lines have to be on top of circles.
		 */
		a = 0;
		for (var i = 0; i < circles.length; i++) {
			var xpos = x;
			var ypos = y - circles[i];

			var lineLength = xpos + x;
			var textXPos = xpos + x + 5;
			a += 1;
			var textYPos = a * fontSize + legendTitleFontSize + 2;
			var lineEndYPos = textYPos - (fontSize / 2);
			var lineStartYPos = ypos - circles[i];
			svgHTML += '<line x1="' + xpos + '" y1="' + lineStartYPos
					+ '" x2="' + lineLength + '" y2="' + lineEndYPos
					+ '" style="stroke:rgb(0,0,0);stroke-width:1" />';
		}

		svgHTML += '</g></svg>';

		div.innerHTML = svgHTML;

		return div;
	};

	return legendCircle;
}

function createPieChart(latlng, attributeData, chartSize, legendAttribute,legendUnits) {

	var colorValue = Math.random() * 360;
	var options = {
		color : '#000',
		weight : 1,
		fillColor : 'hsl(' + colorValue + ',100%,50%)',
		radius : chartSize,
		fillOpacity : 0.7,
		rotation : 0.0,
		position : {
			x : 0,
			y : 0
		},
		offset : 0,
		numberOfSides : 50,
		barThickness : 10
	};

	options.data = {};
	options.chartOptions = {};
	for ( var category in attributeData.categories) {
		var option = {};
		option['fillColor'] = attributeData.categories[category].legendColor;
		option['minValue'] = 0;
		option['maxValue'] = 20;
		option['maxHeight'] = 20;

		var label;
		
		for (attribute in legendAttribute) {
			if (legendAttribute[attribute].attributeid == attributeData.categories[category].attributeid) {
				option['fillColor'] = legendAttribute[attribute].color;
				label = legendAttribute[attribute].label;
				option['displayText'] = function(value) {
					return String(value.toFixed(2)) + ' ' + legendUnits;
				};

				/*
				 * Example: options.data = { 'Věkový stupeň 1': Math.random() *
				 * 20, 'Věkový stupeň 2': Math.random() * 20 };
				 */
				options.data[label] = attributeData.categories[category].point_estimate;
			}
		}

		/*
		 * Example: // options.chartOptions = { // 'Věkový stupeň 1': { //
		 * fillColor: '#8BD8D2', // minValue: 0, // maxValue: 20, // maxHeight:
		 * 20, // displayText: function (value) { // return value.toFixed(2); // } // } // };
		 */
		options.chartOptions[label] = option;
	}

	// Tooltip size
	options.tooltipOptions = {
		iconSize : new L.Point(100, 60),
		iconAnchor : new L.Point(-4, 76)
	};
	
	return new L.PieChartMarker(latlng, options);
}