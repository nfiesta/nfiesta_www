#!/bin/bash

main() {
	if [ "$1" = 'postgres' ] ; then
		# setup data directories and permissions (when run as root)
		declare -g DATABASE_ALREADY_EXISTS
		# look specifically for PG_VERSION, as it is expected in the DB dir
		if sudo test -s "$PGDATA/PG_VERSION" ; then
				DATABASE_ALREADY_EXISTS='true'
		fi

		# only run initialization on an empty data directory
		if [ -z "$DATABASE_ALREADY_EXISTS" ]; then
			exec /nfiesta/init.sh

			cat <<-'EOM'

				PostgreSQL init process complete; ready for start up.

			EOM
		else
			cat <<-'EOM'

				PostgreSQL Database directory appears to contain a database; Skipping initialization

			EOM
		fi

		sudo -u postgres "$@"
	else
		exec "$@"
	fi
}

main "$@"
