#!/bin/sh

sudo chown postgres $PGDATA

# start postgres
sudo -u postgres /usr/lib/postgresql/12/bin/initdb -D  $PGDATA
sudo service postgresql start
# create DB user (preferably with same name as Linux user to bypass password provision)
sudo -u postgres psql -c "CREATE USER vagrant SUPERUSER;"
sudo -u postgres psql -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"
sudo -u postgres psql -c "CREATE ROLE adm_nfiesta; CREATE ROLE app_nfiesta; CREATE ROLE app_nfiesta_mng; GRANT app_nfiesta TO app_nfiesta_mng;"
# build install extensions
# ===============================nfiesta_results========================
# no need to clone, sources already clonned in current directory (/builds)
cd /builds/nfiesta/nfiesta_results && sudo make install

sudo -u postgres psql -c "CREATE EXTENSION postgis ;"

sudo -u postgres psql -c "DROP EXTENSION IF EXISTS nfiesta_results;
                          CREATE EXTENSION nfiesta_results;
                          ALTER EXTENSION nfiesta_results UPDATE TO '${NFIESTA_VERSION}';
"

# default data
sudo -u postgres psql postgres < /builds/nfiesta/nfiesta_results/sql/import_data.sql
sudo -u postgres psql postgres < /builds/nfiesta/nfiesta_results/sql/api_fce.sql
# stop postgres
sudo service postgresql stop
