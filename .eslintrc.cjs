module.exports = {
	"env": {
		"browser": true,
		"es6": true
	},
	"extends": [
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking"
	],
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"project": "tsconfig.json",
		"sourceType": "module"
	},
	"plugins": [
		"@typescript-eslint",
		"import",
		"prefer-arrow"
	],
	"settings": {
		"import/resolver": {
			"typescript": {
				"project": "."
			}
		}
	},
	"rules": {
		"@typescript-eslint/array-type": "error",
		"@typescript-eslint/consistent-type-definitions": "error",
		"@typescript-eslint/explicit-member-accessibility": [
			"error",
			{
				"accessibility": "explicit"
			}
		],
		"@typescript-eslint/indent": [
			"error",
			"tab",
			{
				"FunctionDeclaration": {
					"parameters": "first"
				},
				"FunctionExpression": {
					"parameters": "first"
				},
				"SwitchCase": 1,
			}
		],
		"@typescript-eslint/member-delimiter-style": [
			"error",
			{
				"multiline": {
					"delimiter": "semi",
					"requireLast": true
				},
				"singleline": {
					"delimiter": "semi",
					"requireLast": false
				}
			}
		],
		"@typescript-eslint/member-ordering": [
			"error",
			{
				"default": [
					// Fields
					'static-field', // = ['public-static-field', 'protected-static-field', 'private-static-field'])
					'instance-field', // = ['public-instance-field', 'protected-instance-field', 'private-instance-field'])
					'abstract-field', // = ['public-abstract-field', 'protected-abstract-field', 'private-abstract-field'])

					// Constructors
					'constructor', // = ['public-constructor', 'protected-constructor', 'private-constructor'])

					// Methods
					'static-method', // = ['public-static-method', 'protected-static-method', 'private-static-method'])
					'instance-method', // = ['public-instance-method', 'protected-instance-method', 'private-instance-method'])
					'abstract-method', // = ['public-abstract-method', 'protected-abstract-method', 'private-abstract-method'])
				]
			}
		],
		"@typescript-eslint/naming-convention": [
			"error",
			{
				"selector": "interface",
				"format": ["PascalCase"],
				"custom": {
					"regex": "^I[A-Z]",
					"match": true
				}
			}
		],
		"@typescript-eslint/no-explicit-any": "error",
		"@typescript-eslint/no-parameter-properties": "off",
		"@typescript-eslint/no-use-before-define": "off",
		"@typescript-eslint/no-shadow": ["error"],
		"@typescript-eslint/prefer-for-of": "error",
		"@typescript-eslint/prefer-function-type": "error",
		"@typescript-eslint/quotes": [
			"error",
			"double",
			{
				"avoidEscape": true
			}
		],
		"@typescript-eslint/no-unused-vars": [
			"error",
			{
				"argsIgnorePattern": "^_$"
			}
		],
		"@typescript-eslint/semi": [
			"error",
			"always"
		],
		"@typescript-eslint/triple-slash-reference": "error",
		"@typescript-eslint/unified-signatures": "error",
		"arrow-body-style": "error",
		"arrow-parens": [
			"error",
			"as-needed"
		],
		"brace-style": "error",
		"camelcase": "error",
		"comma-dangle": [
			"error",
			"always-multiline"
		],
		"complexity": "off",
		"constructor-super": "error",
		"curly": "error",
		"dot-notation": "error",
		"eol-last": "error",
		"eqeqeq": [
			"error",
			"smart"
		],
		"guard-for-in": "error",
		"id-blacklist": [
			"error",
			"any",
			"Number",
			"number",
			"String",
			"string",
			"Boolean",
			"boolean",
			"Undefined",
		],
		"id-match": "error",
		"import/order": ["error", {
			"groups": ["builtin", "external", "internal", "parent", "sibling", "index"],
			"alphabetize": {"order": "asc", "caseInsensitive": false},
			"newlines-between": "always",
		}],
		"sort-imports": ["error", {
			"ignoreDeclarationSort": true,
		}],
		"import/no-unassigned-import": "error",
		"import/no-default-export": "error",
		"indent": "off",
		"max-classes-per-file": [
			"error",
			1
		],
		"max-len": [
			"error",
			{
				"code": 120
			}
		],
		"new-parens": "error",
		"no-bitwise": "error",
		"no-caller": "error",
		"no-cond-assign": "error",
		"no-console": "error",
		"no-debugger": "error",
		"no-empty": "error",
		"no-eval": "error",
		"no-fallthrough": "off",
		"no-invalid-this": "error",
		"no-multiple-empty-lines": "error",
		"no-new-wrappers": "error",
		"no-shadow": "off",
		"no-throw-literal": "error",
		"no-trailing-spaces": "error",
		"no-undef-init": "error",
		"no-underscore-dangle": "off",
		"no-unsafe-finally": "error",
		"no-unused-expressions": "error",
		"no-unused-labels": "error",
		"object-shorthand": "error",
		"one-var": [
			"error",
			"never"
		],
		"prefer-arrow/prefer-arrow-functions": "error",
		"quote-props": [
			"error",
			"consistent-as-needed"
		],
		"radix": "error",
		"space-before-function-paren": [
			"error",
			{
				"anonymous": "never",
				"asyncArrow": "always",
				"named": "never"
			}
		],
		"spaced-comment": ["error", "always", {
			"markers": ["!"]
		}],
		"use-isnan": "error",
		"valid-typeof": "off"
	},
	"overrides": [
		{
			"files": ["*.spec.ts"],
			"rules": {
				"id-blacklist": [
					"error",
					"any",
					"Number",
					"number",
					"String",
					"string",
					"Boolean",
					"boolean",
					"Undefined",
				],
				"dot-notation": "off",
				"@typescript-eslint/unbound-method": "off",
				"max-classes-per-file": "off",
			}
		},
		{
			"files": ["index.ts"],
			"rules": {
				"import/no-internal-modules": "off",
				"max-len": "off",
			}
		}
	]
};
