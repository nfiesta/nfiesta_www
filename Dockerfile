FROM php:7.3-fpm-alpine AS php-builder
MAINTAINER Ondřej Václavík <ahoj@ondrejvaclavik.cz>

RUN apk --no-cache add postgresql-dev libzip-dev
RUN docker-php-ext-install opcache pdo_pgsql pgsql zip \
    && docker-php-source delete

FROM php:7.3-fpm-alpine as production

MAINTAINER Ondřej Václavík <ahoj@ondrejvaclavik.cz>

RUN apk --no-cache update && apk add --no-cache \
	# server
		apache2 \
		apache2-proxy \
		runit \
	# php
		postgresql \
		libzip \
		zip \
	# clean up
	&& rm -rf /var/cache/apk/*

COPY --from=php-builder /usr/local/lib/php/extensions/ /usr/local/lib/php/extensions/
COPY --from=php-builder /usr/local/etc/php/conf.d/ /usr/local/etc/php/conf.d/

COPY docker/app/production/ /
COPY src /var/www/html

RUN set -x \
	&& sed -i 's/^LoadModule proxy_fdpass_module/#LoadModule proxy_fdpass_module/' /etc/apache2/conf.d/proxy.conf \
	&& sed -i '/LoadModule rewrite_module/s/^#//g' /etc/apache2/httpd.conf \
	&& ln -sf /proc/self/fd/1 /var/log/apache2/access.log \
	&& ln -sf /proc/self/fd/1 /var/log/apache2/error.log

RUN chmod 755 /sbin/*.sh \
	&& chmod 755 /etc/service/**/*

EXPOSE 80
EXPOSE 443

WORKDIR /app
CMD ["/sbin/runsvdir-wrapper.sh"]

FROM production as development

COPY docker/app/development/ /
